package toolset

import (
	"bytes"
	"encoding/base64"
	"gitee.com/zrj99123/common/pkg/util/file"
	"io/ioutil"
	"log"
	"net/smtp"
	"strings"
	"time"
)

type Mail struct {
	User     string
	Password string
	Host     string
	Port     string
	AuthInfo smtp.Auth
}

type Attachment struct {
	FilePath    string
	ContentType string
	WithFile    bool
}

type Message struct {
	From        string     //发送者
	To          []string   //接收者
	Cc          []string   //抄送
	Bcc         []string   //密送
	Subject     string     //主题
	Body        string     //邮件发送的内容
	ContentType string     //内容所属类型
	Attachment  Attachment //附件
}

func (mail *Mail) SendMail(mailtype, filePath, subject, body string, toUser, Cc, Bcc []string, withFile bool) error {
	bodyContentType := "text/plain;charset=utf-8"
	if mailtype == "html" {
		bodyContentType = "text/html;charset=utf-8"
	}

	contentType := ""
	suffix := file.GetFileSuffix(filePath)
	switch suffix {
	case ".xls":
		contentType = "application/vnd.ms-excel"
	case ".xlsx":
		contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
	case ".docx":
		contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
	default:
		contentType = "text/plain"
	}

	message := Message{
		From:        mail.User,
		To:          toUser,
		Cc:          Cc,
		Bcc:         Bcc,
		Subject:     subject,
		Body:        body,
		ContentType: bodyContentType,
		Attachment: Attachment{
			FilePath:    filePath,
			ContentType: contentType,
			WithFile:    withFile,
		},
	}

	err := mail.send(message)
	if err != nil {
		return err
	}
	return nil
}

func (mail *Mail) auth() {
	mail.AuthInfo = smtp.PlainAuth("", mail.User, mail.Password, mail.Host)
}

func (mail *Mail) send(message Message) error {
	mail.auth()
	buffer := bytes.NewBuffer(nil)
	boundary := "GoBoundary"
	Header := make(map[string]string)
	Header["From"] = message.From
	Header["To"] = strings.Join(message.To, ";")
	Header["Cc"] = strings.Join(message.Cc, ";")
	Header["Bcc"] = strings.Join(message.Bcc, ";")
	Header["Subject"] = message.Subject
	Header["Content-Type"] = "multipart/mixed;boundary=" + boundary
	Header["Mime-Version"] = "1.0"
	Header["Date"] = time.Now().String()
	mail.writeHeader(buffer, Header)

	body := "\r\n--" + boundary + "\r\n"
	body += "Content-Type:" + message.ContentType + "\r\n"
	body += "\r\n" + message.Body + "\r\n"
	buffer.WriteString(body)

	if message.Attachment.WithFile {
		attachment := "\r\n--" + boundary + "\r\n"
		attachment += "Content-Transfer-Encoding:base64\r\n"
		attachment += "Content-Disposition:attachment\r\n"
		attachment += "Content-Type:" + message.Attachment.ContentType + ";name=\"" + file.GetFileName(message.Attachment.FilePath) + "\"\r\n"
		buffer.WriteString(attachment)
		defer func() {
			if err := recover(); err != nil {
				log.Fatalln(err)
			}
		}()
		mail.writeFile(buffer, message.Attachment.FilePath)
	}

	buffer.WriteString("\r\n--" + boundary + "--")
	smtp.SendMail(mail.Host+":"+mail.Port, mail.AuthInfo, message.From, message.To, buffer.Bytes())
	return nil
}

func (mail *Mail) writeHeader(buffer *bytes.Buffer, Header map[string]string) string {
	header := ""
	for key, value := range Header {
		header += key + ":" + value + "\r\n"
	}
	header += "\r\n"
	buffer.WriteString(header)
	return header
}

// read and write the file to buffer
func (mail *Mail) writeFile(buffer *bytes.Buffer, fileName string) {
	file, err := ioutil.ReadFile(fileName)
	if err != nil {
		panic(err.Error())
	}
	payload := make([]byte, base64.StdEncoding.EncodedLen(len(file)))
	base64.StdEncoding.Encode(payload, file)
	buffer.WriteString("\r\n")
	for index, line := 0, len(payload); index < line; index++ {
		buffer.WriteByte(payload[index])
		if (index+1)%76 == 0 {
			buffer.WriteString("\r\n")
		}
	}
}
