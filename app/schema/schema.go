package schema

import "gitee.com/zrj99123/common/pkg/errors"

type StatusText string

func (t StatusText) String() string {
	return string(t)
}

const (
	OKStatus    StatusText = "OK"
	ErrorStatus StatusText = "ERROR"
	FailStatus  StatusText = "FAIL"
)

type StatusResult struct {
	Status StatusText `json:"status"`
}

type ErrorResult struct {
	Error ErrorItem `json:"error"`
}

type ErrorItem struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type Result errors.Response

// UserQueryOptions 查询可选参数项
type QueryOptions struct {
	OrderFields  []*OrderField
	SelectFields []string
	Conditions   []string
}

type ListResult struct {
	List       interface{}       `json:"list"`
	Pagination *PaginationResult `json:"pagination_result,omitempty"`
}

type PaginationResult struct {
	Total    int64 `json:"total_count"`
	Current  int   `json:"page_index"`
	PageSize int   `json:"page_count"`
}

type PaginationParam struct {
	Pagination bool `form:"-"`
	OnlyCount  bool `form:"-"`
	PageIndex  int  `form:"page_index" binding:"required"`
	PageSize   int  `form:"page_size" binding:"required,max=200"`
}

func (a PaginationParam) GetCurrent() int {
	return a.PageIndex
}

func (a PaginationParam) GetPageSize() int {
	pageSize := a.PageSize
	if a.PageSize <= 0 {
		pageSize = 100
	}
	return pageSize
}

type OrderDirection int

const (
	OrderByASC OrderDirection = iota + 1
	OrderByDESC
)

// Create order fields key and define key index direction
func NewOrderFieldWithKeys(keys []string, directions ...map[int]OrderDirection) []*OrderField {
	m := make(map[int]OrderDirection)
	if len(directions) > 0 {
		m = directions[0]
	}

	fields := make([]*OrderField, len(keys))
	for i, key := range keys {
		d := OrderByASC
		if v, ok := m[i]; ok {
			d = v
		}

		fields[i] = NewOrderField(key, d, "")
	}

	return fields
}

func NewOrderFields(orderFields ...*OrderField) []*OrderField {
	return orderFields
}

func NewOrderField(key string, d OrderDirection, n ...string) *OrderField {

	var nullDirection string
	if len(n) > 0 {
		nullDirection = n[0]
	}
	return &OrderField{
		Key:           key,
		Direction:     d,
		NullDirection: nullDirection,
	}
}

type OrderField struct {
	Key           string
	Direction     OrderDirection
	NullDirection string
}

func NewIDResult(id uint64) *IDResult {
	return &IDResult{
		ID: id,
	}
}

type IDResult struct {
	ID uint64 `json:"id,string"`
}
