package days

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
)

type Days struct {
	Id    string `json:"id" gorm:"column:id"`       // 账户ID
	Type  int    `json:"type" gorm:"column:type"`   // 类型(1：工作日,2：节假日)
	Year  int    `json:"year" gorm:"column:year"`   // 年
	Month int    `json:"month" gorm:"column:month"` // 月
	Day   int    `json:"day" gorm:"column:day"`     // 日

	util.Model
}

func GetDaysDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Days))
}

func (Days) TableName() string {
	return "tbl_days"
}
