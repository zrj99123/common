package opportunities

//
//type Opportunities struct {
//	OpportunitiesId       string    `json:"op_id" gorm:"column:op_id;type:uuid"`
//	CtcId                 string    `json:"client_id" gorm:"column:ctc_id;type:uuid"`
//	Property              int       `json:"property" gorm:"column:property;type:smallint"`
//	InIndustries          string    `json:"in_industries" gorm:"column:in_industries;type:json"`
//	DecisionRight         bool      `json:"decision_right" gorm:"column:decision_right;type:bool"`
//	LikeIndustries        string    `json:"like_industries" gorm:"column:like_industries;type:json"`
//	LikeSecType           int       `json:"like_sec_type" gorm:"column:like_sec_type;type:smallint"`
//	RiskAppetite          int       `json:"risk_appetite" gorm:"column:risk_appetite;type:smallint"`
//	LiquidityRequirements int       `json:"liquidity_requirements" gorm:"column:liquidity_requirements;type:smallint"`
//	SecId                 string    `json:"sec_id" gorm:"column:sec_id;type:uuid"`
//	PrjId                 string    `json:"prj_id" gorm:"column:prj_id;type:uuid"`
//	ExpectDay             time.Time `json:"expect_day" gorm:"column:expect_day;type:date"`
//	IsPaid                bool      `json:"is_paid" gorm:"column:is_paid;type:bool"`
//	Amount                float64    `json:"amount" gorm:"column:amount;type:numeric"`
//	CreateTime            time.Time `json:"create_time" gorm:"column:create_time;type:date"` // 创建时间
//	LastUpdate            time.Time `json:"last_update" gorm:"column:last_update;type:date"` // 最后一次更新时间
//	Active                int       `json:"active" gorm:"column:active;type:smallint"`       // 0表示已删除，1表示有效;默认为1
//}
