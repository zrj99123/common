package employees

import "gitee.com/zrj99123/common/app/dao/util"

type Employees struct {
	Id            string `json:"id"  gorm:"column:id" `                         //主键id
	UserId        string `json:"userid"  gorm:"column:userid" `                 //钉钉userid
	Name          string `json:"name"  gorm:"column:name" `                     //姓名
	Mobile        string `json:"mobile" gorm:"column:mobile"`                   //手机
	Title         string `json:"title" gorm:"column:title"`                     //岗位
	HiredDate     string `json:"hired_date" gorm:"column:hired_date"`           //入职时间
	DeptId        int    `json:"dept_id" gorm:"column:dept_id"`                 //部门编码
	DeptNameShort string `json:"dept_name_short" gorm:"column:dept_name_short"` //部门简称
	TagCategory   int    `json:"tag_category" gorm:"column:tag_category"`       //团队分类, 1=总部股东及管理层, 2=中心负责人 3=新投顾
	TagAdvisor    int    `json:"tag_advisor" gorm:"column:tag_advisor"`         //是否投顾, 1=投顾
	LastDay       string `json:"last_day" gorm:"column:last_day"`               //离职日期
	MainDeptId    int    `json:"main_dept_id" gorm:"column:main_dept_id"`       //主部门id
	MainDeptName  string `json:"main_dept_name" gorm:"column:main_dept_name"`   //主部门名称
	CtcId         string `json:"ctc_id" gorm:"column:ctc_id"`                   //联系人id 关联tbl_contacts表
	OrgId         string `json:"org_id" gorm:"column:org_id"`                   //组织架构id 关联organization表
	util.Model
}
