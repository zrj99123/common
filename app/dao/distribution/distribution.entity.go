package distribution

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
)

type Distribution struct {
	DistId  string  `json:"dist_id,omitempty" gorm:"column:dist_id"`   // 主键ID
	OrderId string  `json:"order_id,omitempty" gorm:"column:order_id"` // 订单/库存ID
	Type    int     `json:"type,omitempty" gorm:"column:type"`         //
	CtcId   string  `json:"ctc_id,omitempty" gorm:"column:ctc_id"`     // 投顾/分公司ID
	Ratio   float64 `json:"ratio,omitempty" gorm:"column:ratio"`       // 占比
	Role    int     `json:"role,omitempty" gorm:"column:role"`         //角色

	util.Model
}

func GetDistributionDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Distribution))
}

func (Distribution) TableName() string {
	return "tbl_distribution"
}
