package qualification

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
	"time"
)

type Qualification struct {
	ID           string    `json:"id" gorm:"column:id"`
	CtcId        string    `json:"ctc_id" gorm:"column:ctc_id"`
	Type         int       `json:"type" gorm:"column:type"`
	IssuerId     string    `json:"issuer_id" gorm:"column:issuer_id"`
	EffectiveDay time.Time `json:"effective_day" gorm:"column:effective_day"`
	ExpiredDay   time.Time `json:"expired_day" gorm:"column:expired_day"`

	util.Model
}

func GetQualificationDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Qualification))
}

func (Qualification) TableName() string {
	return "tbl_qualification"
}
