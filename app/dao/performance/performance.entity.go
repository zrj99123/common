package performance

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
	"time"
)

func GetPerformanceDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Performance))
}

type Performance struct {
	ObjectId           string    `json:"object_id" gorm:"column:object_id"`                       // Contact/Account/Security ID
	AsOfDate           time.Time `json:"as_of_date" gorm:"column:as_of_date"`                     // 截止日期
	MarketValue        float64   `json:"mark_value" gorm:"column:market_value"`                   // 市值
	PeriodTwr          float64   `json:"period_twr" gorm:"column:period_twr"`                     // 区间收益率，和上一次估值日比较
	CumulativeTwr      float64   `json:"cumulative_twr" gorm:"column:cumulative_twr"`             // 累计收益率，和初始日比较
	AnnualizedReturn   float64   `json:"annualized_return" gorm:"column:annualized_return"`       // 年化收益率，和初始日比较
	NetInvestment      float64   `json:"net_investment" gorm:"column:net_investment"`             // 累计净投入，和初始日比较
	UnrealizedGainLoss float64   `json:"unrealized_gain_loss" gorm:"column:unrealized_gain_loss"` // 浮动收益
	TotalGainLoss      float64   `json:"total_gain_loss" gorm:"column:total_gain_loss"`           // 累计总收益
	TotalCost          float64   `json:"total_cost" gorm:"column:total_cost"`                     // 存量本金（总成本）
	CashDividend       float64   `json:"cash_dividend" gorm:"column:cash_dividend"`               // 累计现金分红
	Twr1m              float64   `json:"twr_1m" gorm:"column:twr_1m"`                             // 近1月收益率（时间加权法）
	Twr3m              float64   `json:"twr_3m" gorm:"column:twr_3m"`                             // 近3月收益率（时间加权法）
	Twr6m              float64   `json:"twr_6m" gorm:"column:twr_6m"`                             // 近半年收益率（时间加权法）
	Twr1y              float64   `json:"twr_1y" gorm:"column:twr_1y"`                             // 近1年收益率（时间加权法）
	Twr3y              float64   `json:"twr_3y" gorm:"column:twr_3y"`                             // 近3年收益率（时间加权法）
	Twr5y              float64   `json:"twr_5y" gorm:"column:twr_5y"`                             // 近5年收益率（时间加权法）

	util.Model
}

func (Performance) TableName() string {
	return "tbl_performance"
}
