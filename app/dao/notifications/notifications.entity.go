package notifications

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
	"time"
)

type Notifications struct {
	NtfiID       string    `json:"ntfi_id" gorm:"column:ntfi_id"`             // 主键ID
	Type         int       `json:"type" gorm:"column:type"`                   // 1 客户生日 2 产品开放申购 3 产品开放赎回 4 净值更新 5 打款提醒
	Title        string    `json:"title" gorm:"column:title"`                 // 标题/名称
	EffectiveDay time.Time `json:"effective_day" gorm:"column:effective_day"` // 生效日期
	Content      string    `json:"content" gorm:"column:content"`             // 说明/内容
	HaveRead     int       `json:"have_read" gorm:"column:have_read"`         // 0表示未读，1表示已读；默认为0
	TargetId     string    `json:"target_id" gorm:"column:target_id"`         // 推送目标ID
	SendDate     time.Time `json:"send_date" gorm:"column:send_date"`         // 推送日期
	util.Model
}

func (n *Notifications) BeforeCreate(tx *gorm.DB) (err error) {

	_ = n.Model.BeforeCreate(tx)

	n.HaveRead = 0

	return nil
}

func GetNotificationsDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Notifications))
}

func (Notifications) TableName() string {
	return "tbl_notification"
}
