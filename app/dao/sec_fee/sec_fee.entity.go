package sec_fee

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
)

type SecFee struct {
	FeeId   string  `json:"fee_id" gorm:"column:fee_id"`     // 主键ID
	SecId   string  `json:"sec_id" gorm:"column:sec_id"`     // 产品ID
	FeeType int     `json:"fee_type" gorm:"column:fee_type"` // 主键ID 1. 认购费 2. 申购费 3. 赎回费 4. 销售服务费 5. 投资顾问费 7. 托管费 8. 外包服务费 9. 管理费 10. 行政费 11. 业绩报酬(浮动管理费）
	Ratio   float64 `json:"ratio" gorm:"column:ratio"`       // 费率
	Remark  string  `json:"remark" gorm:"column:remark"`     // 备注

	util.Model
}

func GetSecFeeDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(SecFee))
}

func (SecFee) TableName() string {
	return "tbl_secfee"
}
