package transaction

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
	"time"
)

func GetTransactionDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Transaction))
}

type Transaction struct {
	TradeId    string    `json:"trade_id" gorm:"column:trade_id"`       // 交易ID
	CtcId      string    `json:"ctc_id" gorm:"column:ctc_id"`           // 客户ID
	AcctId     string    `json:"acct_id" gorm:"column:acct_id"`         // 账户ID
	SecId      string    `json:"sec_id" gorm:"column:sec_id"`           // 产品ID
	TradeType  int       `json:"trade_type" gorm:"column:trade_type"`   // 交易类型，见字典_TradeType
	Price      float64   `json:"price" gorm:"column:price"`             // 交易价格/净值
	Quantity   float64   `json:"quantity" gorm:"column:quantity"`       // 数量
	Amount     float64   `json:"amount" gorm:"column:amount"`           // 金额
	Charge     float64   `json:"charge" gorm:"column:charge"`           // 手续费
	NetAmount  float64   `json:"net_amount" gorm:"column:net_amount"`   // 净额
	TradeDay   time.Time `json:"trade_day" gorm:"column:trade_day"`     // 交易日期
	OriginalId string    `json:"original_id" gorm:"column:original_id"` // 原交易ID

	util.Model
}

func (Transaction) TableName() string {
	return "tbl_transaction"
}
