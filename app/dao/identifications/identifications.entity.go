package identifications

import (
	"context"
	"fmt"
	"gitee.com/zrj99123/common/app/dao/util"
	"github.com/golang-module/carbon/v2"
	"gorm.io/gorm"
)

type Identifications struct {
	ID           string `json:"id,omitempty" gorm:"column:id"`
	CtcID        string `json:"ctc_id,omitempty" gorm:"column:ctc_id"`
	Type         int    `json:"type,omitempty" gorm:"column:type"`                   // 联系人类型,字典见_IdentificationType
	Number       string `json:"number,omitempty" gorm:"column:number"`               // 关系,第一人如何介绍第三人，字典见_Relation
	ExpiredDay   string `json:"expired_day,omitempty" gorm:"column:expired_day"`     // 		到期日
	EffectiveDay string `json:"effective_day,omitempty" gorm:"column:effective_day"` //        生效日
	Address      string `json:"address,omitempty" gorm:"column:address"`             // 		身份证地址

	util.Model
}

func (n *Identifications) BeforeCreate(tx *gorm.DB) (err error) {

	_ = n.Model.BeforeCreate(tx)

	if n.ExpiredDay == "" {
		n.ExpiredDay = carbon.Parse("2099-01-01 00:00:00").ToDateTimeString()
	}

	// call addidentification('a6a1edac-4264-4690-afa3-6839a46b010d'::uuid,103::smallint,'421102198704120857'::character varying,'2022-08-23'::date,NULL);
	go tx.Exec(fmt.Sprintf("CALL addidentification("+
		"'%v'::uuid,"+
		"%v::smallint,"+
		"'%v'::character varying,"+
		"'%v'::date,"+
		"NULL);",
		n.CtcID,
		n.Type,
		n.Number,
		n.ExpiredDay,
	))

	return nil
}

func (n *Identifications) BeforeUpdate(tx *gorm.DB) (err error) {

	_ = n.Model.BeforeUpdate(tx)

	if n.ExpiredDay == "" {
		n.ExpiredDay = "2099-01-01"
	}

	return nil
}

func GetIdentificationsDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Identifications))
}

func (Identifications) TableName() string {
	return "tbl_identification"
}
