package organization

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
)

type Organization struct {
	OrgId    string `json:"org_id" gorm:"column:org_id"`       // 主键ID
	Name     string `json:"name" gorm:"column:name"`           // 组织机构名称
	ParentId string `json:"parent_id" gorm:"column:parent_id"` // 上层机构ID
	CtcId    string `json:"ctc_id" gorm:"column:ctc_id"`       // Contacts.ctc_id, 有独立法人或联系方式的，在Contacts里存一份冗余，如”分中心“

	util.Model
}

func GetOrganizationDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Organization))
}

func (Organization) TableName() string {
	return "tbl_organization"
}
