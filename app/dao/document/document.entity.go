package document

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
)

type Document struct {
	DocId    string `json:"doc_id" gorm:"doc_id"`       // id
	FileId   string `json:"file_id" gorm:"file_id"`     // 文件名
	ContType int    `json:"cont_type" gorm:"cont_type"` // 内容类型，见字典_DocType
	util.Model
}

func GetDocumentDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Document))
}

func (Document) TableName() string {
	return "tbl_document"
}

type DocumentHistory struct {
	FileId   string `json:"file_id" gorm:"file_id"`    // 文件名
	UserId   string `json:"user_id" gorm:"user_id"`    // 用户名
	DocId    string `json:"doc_id" gorm:"doc_id"`      // id
	Version  int    `json:"version" gorm:"version"`    // version
	Name     string `json:"name" gorm:"name"`          // 文件名
	Label    string `json:"label" gorm:"label"`        // 文件标签
	FileType int    `json:"file_type" gorm:"doc_type"` // 文件类型，见字典_DocType
	Size     int64  `json:"size" gorm:"size"`          // 大小,以B为单位
	Path     string `json:"path" gorm:"column:path"`   // 路径(源文件是文件夹)

	AuditStatus int    `json:"audit_status" gorm:"audit_status"` // 1. 未审核 2. 通过 3. 驳回
	AuditDesc   string `json:"audit_desc" gorm:"audit_desc"`

	util.Model
}

func GetDocumentHistoryDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(DocumentHistory))
}

func (DocumentHistory) TableName() string {
	return "tbl_document_history"
}
