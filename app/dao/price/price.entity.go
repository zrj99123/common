package price

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	cheretechTime "gitee.com/zrj99123/common/pkg/util/time"
	"gorm.io/gorm"
	"time"
)

func GetPriceDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Price))
}

type Price struct {
	PriceID      string    `json:"pr_id" gorm:"primaryKey;column:pr_id"`      // 价格ID
	SecID        string    `json:"sec_id" gorm:"column:sec_id"`               // 产品ID
	AsOfDate     time.Time `json:"as_of_date" gorm:"column:as_of_date"`       // 净值日
	Price        float64   `json:"price" gorm:"column:price"`                 // 价格/净值
	AjustedPrice float64   `json:"ajusted_price" gorm:"column:ajusted_price"` // 调整后价格/累计净值
	CreateTime   time.Time `json:"create_time" gorm:"column:create_time"`     // 创建时间
	LastUpdate   time.Time `json:"last_update" gorm:"column:last_update"`     // 最后更新时间
	Active       int       `json:"active" gorm:"column:active"`               // 0表示已删除，1表示有效;默认为1
}

func (Price) TableName() string {
	return "tbl_price"
}

type GetSecuritiesPriceItem struct {
	Price           float64 `json:"price" gorm:"column:price"`                     // 单位净值(保留四位小数)
	PriceDate       string  `json:"price_date" gorm:"column:as_of_date"`           // 发布日期
	AdjustedPrice   float64 `json:"adjusted_price" gorm:"column:adjusted_price"`   // 累计净值（保留四位小数）
	NavFloating     float64 `json:"nav_floating" gorm:"column:nav_floating"`       // 收益率=净值浮动率(保留两位小数）
	PeriodTwr       float64 `json:"period_twr" gorm:"column:period_twr"`           // 区间收益率(保留两位小数）
	BenchmarkPrice  float64 `json:"benchmark_price" gorm:"column:benchmark_price"` // 沪深300净值（保留四位小数）
	BenchmarkTwr    float64 `json:"benchmark_twr" gorm:"column:benchmark_twr"`     // 沪深300收益率（保留两位小数）
	BenchmarkCumTwr float64 `json:"benchmark_cum_twr" gorm:"-"`                    // 沪深300累计收益率（保留两位小数）
	CumulativeTwr   float64 `json:"cumulative_twr" gorm:"column:cumulative_twr"`   // 累计净值收益率（保留两位小数）
}

type GetSecuritiesPrice struct {
	Price         float64    `json:"price" gorm:"column:price"`                   // 单位净值(保留四位小数)
	PriceDate     *time.Time `json:"price_date" gorm:"column:as_of_date"`         // 发布日期
	AdjustedPrice float64    `json:"adjusted_price" gorm:"column:adjusted_price"` // 累计净值（保留四位小数）
}

func Slice2SecuritiesPriceMap(prices []GetSecuritiesPrice) map[string]GetSecuritiesPrice {
	var (
		m = map[string]GetSecuritiesPrice{}
	)

	for _, v := range prices {
		m[cheretechTime.TimeToDateString(*v.PriceDate)] = v
	}

	return m
}

func (r *GetSecuritiesPriceItem) CalculationSrcPrice(m map[string]GetSecuritiesPrice, endDate *time.Time) {
	if !cheretechTime.IsWeekEndOrHoliday(*endDate) {
		r.Price = m[cheretechTime.TimeToDateString(*endDate)].Price
		r.AdjustedPrice = m[cheretechTime.TimeToDateString(*endDate)].AdjustedPrice
		r.PriceDate = cheretechTime.TimeToDateString(*endDate)

		return
	}

	/*if m[cheretechTime.TimeToDateString(*endDate)].Price != 0 {
		r.Price = m[cheretechTime.TimeToDateString(*endDate)].Price
		r.AdjustedPrice = m[cheretechTime.TimeToDateString(*endDate)].AdjustedPrice
		r.PriceDate = cheretechTime.TimeToDateString(*endDate)

		return
	}*/

	for i := 1; i <= 6; i++ {
		t := endDate.AddDate(0, 0, -1*i)
		if cheretechTime.IsWeekEndOrHoliday(t) {
			continue
		}

		r.Price = m[cheretechTime.TimeToDateString(t)].Price
		r.AdjustedPrice = m[cheretechTime.TimeToDateString(t)].AdjustedPrice
		r.PriceDate = cheretechTime.TimeToDateString(t)
		return

		/*if m[cheretechTime.TimeToDateString(t)].Price != 0 {
			r.Price = m[cheretechTime.TimeToDateString(t)].Price
			r.AdjustedPrice = m[cheretechTime.TimeToDateString(t)].AdjustedPrice
			r.PriceDate = cheretechTime.TimeToDateString(t)

			return
		}*/
	}
}
