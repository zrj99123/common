package correlation

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
)

type Correlation struct {
	ID      string `json:"id" gorm:"column:id"`
	One     string `json:"one" gorm:"column:one"`
	Type    int    `json:"type" gorm:"column:type"`
	Another string `json:"another" gorm:"column:another"`
	Rel     string `json:"rel" gorm:"column:rel"`

	util.Model
}

func GetCorrelationDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Correlation))
}

func (Correlation) TableName() string {
	return "tbl_correlation"
}
