package trade_orders

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
	"time"
)

type TradeOrders struct {
	OrderId   string `json:"order_id,omitempty" gorm:"column:order_id"`     // 商机/预约ID
	CtcId     string `json:"ctc_id,omitempty" gorm:"column:ctc_id"`         // 客户ID
	SecId     string `json:"sec_id,omitempty" gorm:"column:sec_id"`         // 产品ID
	OrderType int    `json:"order_type,omitempty" gorm:"column:order_type"` // 交易类型，见字典_TradeOrderType

	Status      int       `json:"status,omitempty" gorm:"column:status"`             // 状态/感兴趣层度，见字典
	ExpAmount   float64   `json:"exp_amount,omitempty" gorm:"column:exp_amount"`     // 预约金额
	ExpTradeDay time.Time `json:"exp_tradeday,omitempty" gorm:"column:exp_tradeday"` // 预计打款日期
	Amount      float64   `json:"amount,omitempty" gorm:"column:amount"`             // 实际金额
	TradeDay    time.Time `json:"trade_day,omitempty" gorm:"column:trade_day"`       // 实际打款日期

	util.Model
}

func GetTradeOrdersDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(TradeOrders))
}

func (TradeOrders) TableName() string {
	return "tbl_tradeorder"
}
