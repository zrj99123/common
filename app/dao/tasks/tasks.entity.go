package tasks

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
	"time"
)

func GetTasksDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Tasks))
}

type Tasks struct {
	TskId      string    `json:"tsk_id" gorm:"primaryKey;column:tsk_id"` // 价格ID
	Type       int       `json:"type" gorm:"column:type"`                // 任务类型 1生成佣金表格
	StartDay   time.Time `json:"start_day" gorm:"column:start_day"`      // 开始时间
	EndDay     time.Time `json:"end_day" gorm:"column:end_day"`          // 结束时间
	Status     float64   `json:"status" gorm:"column:status"`            // 状态 默认值为1(待开始)
	CreateTime time.Time `json:"create_time" gorm:"column:create_time"`  // 创建时间
	LastUpdate time.Time `json:"last_update" gorm:"column:last_update"`  // 最后更新时间
	Active     int       `json:"active" gorm:"column:active"`            // 0表示已删除，1表示有效;默认为1
}

func (Tasks) TableName() string {
	return "tbl_tasks"
}
