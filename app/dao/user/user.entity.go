package user

import (
	"context"
	"gorm.io/gorm"
	"time"

	"gitee.com/zrj99123/common/app/dao/util"
)

func GetUserDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(User))
}

type User struct {
	UserID       string     `json:"user_id" gorm:"primaryKey;column:user_id"`
	UserName     string     `json:"user_name" gorm:"column:user_name"`         // 用户名
	MobileNumber string     `json:"mobile_number" gorm:"column:mobile_number"` // 用户名
	UnionID      string     `json:"union_id" gorm:"column:union_id"`           // 微信union_id
	Password     string     `json:"password" gorm:"column:password"`           // 密码
	RoleID       string     `json:"role_id" gorm:"column:role_id"`             // 角色ID
	LastLogin    *time.Time `json:"last_login" gorm:"column:last_login"`       // 上次登录时间
	CreateTime   time.Time  `json:"create_time" gorm:"column:create_time"`     // 创建时间
	LastUpdate   time.Time  `json:"last_update" gorm:"column:last_update"`     // 最后更新时间
	Active       int        `json:"active" gorm:"column:active"`               // 是否活跃
}

func (User) TableName() string {
	return "tbl_users"
}

func GetRoleDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Role))
}

type Role struct {
	RoleId   string `json:"role_id" gorm:"primaryKey;column:role_id"` // 角色ID
	UserName string `json:"role_name" gorm:"column:role_name"`        // 角色名称
	SysCode  int    `json:"sys_code" gorm:"column:sys_code"`          // 1. 官网 2. 辰阳小程序(证券) 3. 辰途小程序(股权)  4. 驾驶舱 5.一诺
	DrType   int    `json:"dr_type" gorm:"column:dr_type"`            // 数据权限类型，见字典_DataRights
	DrValue  string `json:"dr_value" gorm:"column:dr_value"`          // 自定义数据权限值
	MrType   int    `json:"mr_type" gorm:"column:mr_type"`            // 菜单权限类型，见字典_MenuRights
	MrValue  string `json:"mr_value" gorm:"column:mr_value"`          // 自定义菜单权限值

	util.Model
}

func (Role) TableName() string {
	return "tbl_roles"
}

type MenuRights struct {
	MrId       string `json:"mr_id" gorm:"primaryKey;column:mr_id"`  // 主键ID
	ApiPath    string `json:"api_path" gorm:"column:api_path"`       // API路径
	ApiMethod  int    `json:"api_method" gorm:"column:api_method"`   // 1. Get 2. POST 3. PUT 4. DELETE
	ButtonPath string `json:"button_path" gorm:"column:button_path"` // 按钮路径
	ButtonName string `json:"button_name" gorm:"column:button_name"` // 按钮名称
	Allowed    bool   `json:"allowed" gorm:"column:allowed"`         // 是否允许访问

	util.Model
}

func GetMenuRightsDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(MenuRights))
}

func (MenuRights) TableName() string {
	return "tbl_menu_rights"
}
