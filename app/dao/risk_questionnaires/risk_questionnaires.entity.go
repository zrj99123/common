package risk_questionnaires

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
	"time"
)

func GetRiskQuestionnairesDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(RiskQuestionnaires))
}

type RiskQuestionnaires struct {
	RtId          string `json:"rt_id" gorm:"column:rt_id"`                 // PK	ID
	Version       string `json:"version" gorm:"column:version"`             // 风险测评问卷版本
	IssuerId      string `json:"issuer_id" gorm:"column:issuer_id"`         // 管理人ID
	CtcId         string `json:"ctc_id" gorm:"column:ctc_id"`               // 客户ID
	OprId         string `json:"opr_id" gorm:"column:opr_id"`               // 操作人ID，按照法律规定，目前只有金融机构才可以认证投资人风险等级
	Score         int    `json:"score" gorm:"column:score"`                 // 测评分数
	Level         string `json:"level" gorm:"column:level"`                 // 风险等级, 1~5
	EffectiveDay  string `json:"effective_day" gorm:"column:effective_day"` // 生效日期
	ExpiredDay    string `json:"expired_day" gorm:"column:expired_day"`     // 到期日
	Questionnaire string `json:"questionnaire" gorm:"column:questionnaire"` // 问卷

	CreateTime time.Time `json:"create_time" gorm:"column:create_time"` // 创建时间
	LastUpdate time.Time `json:"last_update" gorm:"column:last_update"` // 最后一次更新时间
	Active     int       `json:"active" gorm:"column:active"`           // 0表示已删除，1表示有效;默认为1
}

func (RiskQuestionnaires) TableName() string {
	return "tbl_risktolerance"
}
