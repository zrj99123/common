package sec_tradeable

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
	"time"
)

type SecTradeAble struct {
	SecID        string    `json:"sec_id" gorm:"column:sec_id"`               // 产品ID
	Type         int       `json:"type" gorm:"column:type"`                   // 1 表示募集期 2表示固定开放日 3表示临时开放日
	TradeAble    int       `json:"tradeable" gorm:"column:tradeable"`         // 1 表示可买；2表示可卖；3表示可买也可卖
	EffectiveDay time.Time `json:"effective_day" gorm:"column:effective_day"` // 有效日期
	Source       int       `json:"source" gorm:"column:source"`               // 1表示手动添加 2表示自动计算

	util.Model
}

func GetSecTradeAbleDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(SecTradeAble))
}

func (SecTradeAble) TableName() string {
	return "tbl_sectradeable"
}
