package securities

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
	"time"
)

func GetSecuritiesDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Securities))
}

type Securities struct {
	SecID                  string     `json:"sec_id" gorm:"primaryKey;column:sec_id"`
	Name                   string     `json:"name" gorm:"column:name"`                                       // 产品名
	ShortName              string     `json:"short_name" gorm:"column:short_name"`                           // 产品简称
	SecType                int        `json:"sec_type" gorm:"column:sec_type"`                               // 产品类型，字典见_SecType，后期考虑开放自定义
	IssuerId               string     `json:"issuer_id" gorm:"column:issuer_id"`                             // 管理人ID，对应Contact.ctc_id
	InceptionDate          *time.Time `json:"inception_date" gorm:"column:inception_date"`                   // 	成立日
	Duration               int        `json:"duration" gorm:"column:duration"`                               // 	期限（单位：年）
	ExpiryDate             string     `json:"expiry_date" gorm:"column:expiry_date"`                         // 	到期日
	Threshold              int        `json:"threshold" gorm:"column:threshold"`                             // 	投资起点
	Increase               int        `json:"increase" gorm:"column:increase"`                               // 	递增金额
	ClosedPeriod           int        `json:"closed_period" gorm:"column:closed_period"`                     // 	首个封闭期 (单位：月）
	LockPeriod             int        `json:"lock_period" gorm:"column:lock_period"`                         // 	份额锁定期 (单位：月）
	PutonNumber            string     `json:"puton_number" gorm:"column:puton_number"`                       // 	备案编号
	PutonDate              time.Time  `json:"puton_date" gorm:"column:puton_date"`                           // 	备案时间
	CustodianId            string     `json:"custodian_id" gorm:"column:custodian_id"`                       // 	托管人ID
	TaCode                 string     `json:"ta_code" gorm:"column:ta_code"`                                 // 	TA代码
	Description            string     `json:"description" gorm:"column:description"`                         // 	产品简介
	ManagerId              string     `json:"manager_id" gorm:"column:manager_id"`                           // 	基金经理ID
	Scope                  string     `json:"scope" gorm:"column:scope"`                                     // 	投资范围
	Strategy               string     `json:"strategy" gorm:"column:strategy"`                               // 	投资策略
	TradeableType          int        `json:"tradeable_type" gorm:"column:tradeable_type"`                   // 	1 表示定期开放 2 表示不定期开放 3表示不开放
	TradeableDesc          string     `json:"tradeable_desc" gorm:"column:tradeable_desc"`                   // 	开放期说明
	RiskLevel              int        `json:"risk_level" gorm:"column:risk_level"`                           // 	产品风险等级
	OutsourcingInstitution string     `json:"outsourcing_institution" gorm:"column:outsourcing_institution"` // 	外包服务机构
	Status                 int        `json:"status" gorm:"column:status"`                                   // 	1.预热中 2.募集中 3.投资期/首个封闭期 4.封闭运作/管理期 5.开放申购 6.开放赎回 7.开放申赎 8.退出期/清算期 9.已下架 * 其中带“/”的状态描述表示 股权/证券，例如 “投资期/首个封闭期”，描述股权产品时用“投资期”，描述证券产品时用“首个封闭期”
	PublishedClient        bool       `json:"published_client" gorm:"column:published_client"`               // 	1为所有客户可见，0为仅持有客户可见
	PublishedAdvisor       bool       `json:"published_advisor" gorm:"column:published_advisor"`             // 	1为所有投顾可见，0为仅名下客户持有的投顾可见
	CreateTime             time.Time  `json:"create_time" gorm:"column:create_time"`                         // 创建时间
	LastUpdate             time.Time  `json:"last_update" gorm:"column:last_update"`                         // 最后更新时间
	Active                 int        `json:"active" gorm:"column:active"`                                   // 0表示已删除，1表示有效;默认为1
	Tag                    string     `json:"tag" gorm:"column:tag"`                                         // 标签
}

func (Securities) TableName() string {
	return "tbl_securities"
}

type Securitieses []*Securities
