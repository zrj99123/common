package contacts

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
	"time"
)

type Contacts struct {
	CtcID       string    `json:"ctc_id" gorm:"primaryKey;column:ctc_id"`
	CtcType     int       `json:"ctc_type" gorm:"column:ctc_type"`      // 联系人类型,字典见_CTCType
	Name        string    `json:"name" gorm:"column:name"`              // 名称(本地)
	NameEn      string    `json:"name_en" gorm:"column:name_en"`        // 名称(英文)
	FirstName   string    `json:"first_name" gorm:"column:first_name"`  // 名称(本地)
	LastName    string    `json:"last_name" gorm:"column:last_name"`    // 姓氏(本地)
	FirstNameEn string    `json:"last_name_en" gorm:"column:last_name"` // 名字(英文)
	LastNameEn  string    `json:"last_name_en" gorm:"column:last_name"` // 姓氏(英文)
	Gender      string    `json:"gender" gorm:"column:gender"`          // 性别,字典见_Gender
	Birthday    time.Time `json:"birthday" gorm:"column:birthday"`      // 生日
	Age         int       `json:"age" gorm:"column:age"`                // 年龄,通过birthday计算
	Country     string    `json:"country" gorm:"column:country"`        // 国家或地区,ISO 3位国家或地区代码
	Occupation  string    `json:"occupation" gorm:"column:occupation"`  // 职业
	Position    string    `json:"position" gorm:"column:position"`      // 职务

	GraduatedFrom     string    `json:"graduated_from" gorm:"column:graduated_from"`         // 毕业院校
	InstituteName     string    `json:"institute_name" gorm:"column:institute_name"`         // 机构名称
	InstituteType     int       `json:"institute_type" gorm:"column:institute_type"`         // 机构类型, 字典见_InstituteType
	Scope             string    `json:"scope" gorm:"column:scope"`                           // 经营范围, 考虑通过企查查同步
	RegisteredCapital int       `json:"registered_capital" gorm:"column:registered_capital"` // 注册资本, 考虑通过企查查同步
	IcType            int       `json:"ic_type" gorm:"column:ic_type"`                       // 常用证件的证件类型，见字典_IdentificationType
	DefaultMob        string    `json:"default_mob" gorm:"column:default_mob"`               // 常用手机号,加密存储
	DefaultIc         string    `json:"default_ic" gorm:"column:default_ic"`                 // 常用证件号码,加密存储
	DefaultWechat     string    `json:"default_wechat" gorm:"column:default_wechat"`         // 常用微信号码,加密存储
	MultMob           string    `json:"mult_mob" gorm:"column:mult_mob"`                     // 手机号,加密存储
	MultIc            string    `json:"mult_ic" gorm:"column:mult_ic"`                       // 证件,加密存储
	MultIm            string    `json:"mult_im" gorm:"column:mult_im"`                       // 即时通讯,加密存储
	MultAddr          string    `json:"mult_addr" gorm:"column:mult_addr"`                   // 地址,加密存储
	Stage             int       `json:"stage" gorm:"column:stage"`                           // 阶段；字典见_Stage(注：之后会开放给各分中心自定义）
	Source            int       `json:"source" gorm:"column:source"`                         // 客户来源；字典见_Source
	AvatarId          string    `json:"avatar_id" gorm:"column:avatar_id"`                   // 头像ID，联接Document表的doc_id
	IsEmployee        int       `json:"is_employee" gorm:"column:is_employee"`               // 0表示非员工，1表示员工
	DisplayCompany    string    `json:"display_company" gorm:"column:display_company"`       //公司名称
	InceptionDate     time.Time `json:"inception_date" gorm:"column:inception_date"`         //成立日期
	RiskGrade         int       `json:"risk_grade" gorm:"column:risk_grade"`                 // 风险承受能力等级
	InvestorGrade     int       `json:"investor_grade" gorm:"column:investor_grade"`         // 投资者等级

	Categories      int    `json:"categories" gorm:"column:categories"`               // 标签
	Industries      string `json:"industries" gorm:"column:industries"`               // 所在行业
	Property        int    `json:"property" gorm:"column:property"`                   // 可投资产
	GoalLiquidity   int    `json:"goal_liquidity" gorm:"column:goal_liquidity"`       // 流动性
	GoalMaxDrawDown int    `json:"goal_max_drawdown" gorm:"column:goal_max_drawdown"` // 风险偏好
	GoalIndustries  string `json:"goal_industries" gorm:"column:goal_industries"`     // 看好的行业
	GoalProjects    string `json:"goal_projects" gorm:"column:goal_projects"`         // 感兴趣的项目
	GoalSecType     string `json:"goal_sec_type" gorm:"column:goal_sec_type"`         // 倾向的产品类型

	util.Model
}

func GetContactsDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Contacts))
}

func (Contacts) TableName() string {
	return "tbl_contacts"
}
