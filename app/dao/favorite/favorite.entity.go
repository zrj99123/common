package favorite

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
)

type Favorite struct {
	Id      string `json:"id" gorm:"column:id;type:uuid"`                             // 主键ID
	Name    string `json:"name" gorm:"column:name;type:character varying(256)"`       // 主键ID
	UserId  string `json:"user_id" gorm:"column:user_id;type:character varying(128)"` //
	Type    int    `json:"type" gorm:"column:type"`                                   // 收藏夹类型
	Content string `json:"content" gorm:"column:content;type:json"`                   // 收藏夹内容

	util.Model
}

func GetFavoriteDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Favorite))
}

func (Favorite) TableName() string {
	return "tbl_favorite"
}
