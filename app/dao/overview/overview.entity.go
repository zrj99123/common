package overview

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
	"time"
)

func GetHoldingDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Holding))
}

type Holding struct {
	HldgID        string    `json:"hldg_id" gorm:"primaryKey;column:hldg_id"`      //持仓ID
	CtcID         string    `json:"ctc_id" gorm:"column:ctc_id"`                   //客户ID
	AcctID        string    `json:"acct_id" gorm:"column:acct_id"`                 //账户ID
	SecID         string    `json:"sec_id" gorm:"column:sec_id"`                   //产品ID
	Quantity      float64   `json:"quantity" gorm:"column:qualitity"`              //持有份额/数量
	Price         float64   `json:"price" gorm:"column:price"`                     //最新价格/净值
	PriceDate     time.Time `json:"price_date" gorm:"column:price_date"`           //最新价公布日期
	MarketValue   float64   `json:"market_value" gorm:"column:market_value"`       //市值
	FirstTradeDay time.Time `json:"first_trade_day" gorm:"column:first_trade_day"` //首次购买日
	LastTradeDay  time.Time `json:"last_trade_day" gorm:"column:last_trade_day"`   //最后一次购买日

	CreateTime time.Time `json:"create_time" gorm:"column:create_time"` // 创建时间
	LastUpdate time.Time `json:"last_update" gorm:"column:last_update"` // 最后更新时间
}

func (Holding) TableName() string {
	return "tbl_holding"
}
