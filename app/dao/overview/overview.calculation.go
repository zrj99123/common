package overview

import (
	"gitee.com/zrj99123/common/app/dao/price"
	cheretechTime "gitee.com/zrj99123/common/pkg/util/time"
	"time"
)

func CalculationSecuritiesPriceItem(benchmarkPrices, prices []price.GetSecuritiesPrice, endDate *time.Time, inceptionDateStr string) ([]price.GetSecuritiesPriceItem, float64, float64) {
	var (
		resp                            = []price.GetSecuritiesPriceItem{}
		strDate, inceptionDate          *time.Time
		firstPrice, firstBenchmarkPrice float64
		srcPriceMap                     = price.Slice2SecuritiesPriceMap(prices)
	)

	if inceptionDateStr != "" {
		inceptionDate, _ = cheretechTime.DateYMDStringToTime(inceptionDateStr)
	}

	if len(prices) > 0 {
		strDate = prices[len(prices)-1].PriceDate
	}

	if endDate != nil && strDate != nil {
		for endDate.After(*strDate) {
			data := price.GetSecuritiesPriceItem{}
			//若日期为周末或假期(转为最近的一个工作日)
			if cheretechTime.IsWeekEndOrHoliday(*endDate) {
				tmpDay := cheretechTime.GetLastWorkDay(*endDate)
				endDate = &tmpDay
			}

			data.CalculationSrcPrice(srcPriceMap, endDate)
			if data.AdjustedPrice != 0 {
				firstPrice = data.AdjustedPrice
			}

			_, data.BenchmarkPrice = calculationPrice(benchmarkPrices, endDate)
			if data.BenchmarkPrice != 0 && data.AdjustedPrice != 0 {
				firstBenchmarkPrice = data.BenchmarkPrice
			}
			//tmpDate := endDate.AddDate(y, m, -1*d)
			tmpDate := cheretechTime.GetLastlyFriday(*endDate, 1)
			/*//判断日期是否属于周五
			  if endDate.Weekday() != time.Friday {
			  	tmpDate = cheretechTime.GetLastlyFriday(*endDate, 1)
			  }
			  //如果tmpDate 与 endDate不属于同一个月
			  if tmpDate.Month() != endDate.Month() {
			  	//获取tmpDate当月的最后一个工作日
			  	t := cheretechTime.GetMonthLastWorkDay(tmpDate)
			  	tmpDate = *t
			  }*/

			endDate = &tmpDate
			//产品净值不存在的数据 不返回前端
			if data.Price == 0.0 {
				continue
			}
			resp = append(resp, data)
		}

		if inceptionDateStr != "" {
			data := price.GetSecuritiesPriceItem{}

			data.CalculationSrcPrice(srcPriceMap, inceptionDate)
			if data.Price == 0 || data.AdjustedPrice == 0 {
				return resp, firstBenchmarkPrice, firstPrice
			}
			if data.AdjustedPrice != 0 {
				firstPrice = data.AdjustedPrice
			}
			_, data.BenchmarkPrice = calculationPrice(benchmarkPrices, inceptionDate)
			if data.BenchmarkPrice != 0 && data.AdjustedPrice != 0 {
				firstBenchmarkPrice = data.BenchmarkPrice
			}
			resp = append(resp, data)
		}

	}

	return resp, firstBenchmarkPrice, firstPrice
}

func calculationPrice(prices []price.GetSecuritiesPrice, endDate *time.Time) (float64, float64) {
	var price, adjustedPrice float64
	for _, v := range prices {
		if v.PriceDate.After(*endDate) && cheretechTime.TimeToDateString(*v.PriceDate) != cheretechTime.TimeToDateString(*endDate) {
			continue
		}
		price = v.Price
		adjustedPrice = v.AdjustedPrice
		break
	}

	return price, adjustedPrice
}
