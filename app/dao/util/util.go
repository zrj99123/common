package util

import (
	"context"
	"fmt"
	"gitee.com/zrj99123/common/app/schema"
	"gitee.com/zrj99123/common/pkg/errors"
	"strings"
	"time"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"

	"gitee.com/zrj99123/common/app/contextx"
)

// Define base model
type Model struct {
	CreateTime time.Time `json:"create_time" gorm:"column:create_time"` // 创建时间
	LastUpdate time.Time `json:"last_update" gorm:"column:last_update"` // 最后一次更新时间
	Active     int       `json:"active" gorm:"column:active"`           // 0表示已删除，1表示有效;默认为1
}

func (n *Model) BeforeCreate(tx *gorm.DB) (err error) {

	now := time.Now()
	n.CreateTime = now
	n.LastUpdate = now
	n.Active = 1

	tx.Statement.SetColumn("active", 1)
	tx.Statement.SetColumn("create_time", now)
	tx.Statement.SetColumn("last_update", now)

	return
}

func (n *Model) BeforeUpdate(tx *gorm.DB) (err error) {

	now := time.Now()
	n.LastUpdate = now

	tx.Statement.SetColumn("last_update", now)

	return
}

// Get gorm.DB from context
func GetDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	trans, ok := contextx.FromTrans(ctx)
	if ok && !contextx.FromNoTrans(ctx) {
		db, ok := trans.(*gorm.DB)
		if ok {
			if contextx.FromTransLock(ctx) {
				db = db.Clauses(clause.Locking{Strength: "UPDATE"})
			}
			return db
		}
	}

	return defDB
}

// Get gorm.DB.Model from context
func GetDBWithModel(ctx context.Context, defDB *gorm.DB, m interface{}) *gorm.DB {
	return GetDB(ctx, defDB).Model(m)
}

// Define transaction execute function
type TransFunc func(context.Context) error

func ExecTrans(ctx context.Context, db *gorm.DB, fn TransFunc) error {
	transModel := &Trans{DB: db}
	return transModel.Exec(ctx, fn)
}

func ExecTransWithLock(ctx context.Context, db *gorm.DB, fn TransFunc) error {
	if !contextx.FromTransLock(ctx) {
		ctx = contextx.NewTransLock(ctx)
	}
	return ExecTrans(ctx, db, fn)
}

func WrapPageQuery(ctx context.Context, db *gorm.DB, pp schema.PaginationParam, out interface{}) (*schema.PaginationResult, error) {
	if pp.OnlyCount {
		var count int64
		err := db.Count(&count).Error
		if err != nil {
			return nil, err
		}
	} else if !pp.Pagination {
		err := db.Find(out).Error
		return nil, err
	}

	total, err := FindPage(ctx, db, pp, out)
	if err != nil {
		return nil, err
	}

	return &schema.PaginationResult{
		Total:    total,
		Current:  pp.GetCurrent(),
		PageSize: pp.GetPageSize(),
	}, nil
}

func CountOnly(ctx context.Context, db *gorm.DB) (int64, error) {
	var count int64
	err := db.Count(&count).Error
	if err != nil {
		return 0, err
	} else if count == 0 {
		return count, nil
	}
	return count, err
}

func FindPageOnly(ctx context.Context, db *gorm.DB, pp schema.PaginationParam, out interface{}) error {

	current, pageSize := pp.GetCurrent(), pp.GetPageSize()
	if current > 0 && pageSize > 0 {
		db = db.Offset((current - 1) * pageSize).Limit(pageSize)
	} else if pageSize > 0 {
		db = db.Limit(pageSize)
	}

	err := db.Find(out).Error
	return err
}

func FindPage(ctx context.Context, db *gorm.DB, pp schema.PaginationParam, out interface{}) (int64, error) {
	var (
		count    int64
		current  int
		pageSize int
	)
	err := db.Count(&count).Error
	if err != nil {
		return 0, err
	} else if count == 0 {
		return count, nil
	}

	current = pp.GetCurrent()
	pageSize = pp.PageSize

	if current > 0 && pageSize > 0 {
		db = db.Offset((current - 1) * pageSize).Limit(pageSize)
	} else if pageSize > 0 {
		db = db.Limit(pageSize)
	}

	err = db.Find(out).Error
	return count, err
}

func FindPageRaw(ctx context.Context, dbCount *gorm.DB, db *gorm.DB, pp schema.PaginationParam, out interface{}) (int64, error) {
	var count int64
	err := dbCount.Scan(&count).Error
	if err != nil {
		return 0, err
	} else if count == 0 {
		return count, nil
	}

	err = db.Scan(out).Error
	return count, err
}

func FindOne(ctx context.Context, db *gorm.DB, out interface{}) (bool, error) {
	result := db.First(out)
	if err := result.Error; err != nil {
		return false, err
	}
	if result.RowsAffected <= 0 {
		return false, errors.WrapRecordGetErrorResponse(fmt.Sprintf("%+v", errors.RecordNotFound))
	}
	return true, nil
}

func Check(ctx context.Context, db *gorm.DB) (bool, error) {
	var count int64
	result := db.Count(&count)
	if err := result.Error; err != nil {
		return false, err
	}
	return count > 0, nil
}

// Define order fields convert function
type OrderFieldFunc func(string) string

func ParseOrder(items []*schema.OrderField, handle ...OrderFieldFunc) string {
	orders := make([]string, len(items))

	for i, item := range items {
		key := item.Key
		if len(handle) > 0 {
			key = handle[0](key)
		}

		direction := "ASC"
		if item.Direction == schema.OrderByDESC {
			direction = "DESC"
		}

		orders[i] = fmt.Sprintf("%s %s", key, direction+" "+item.NullDirection)
	}

	return strings.Join(orders, ",")
}

// source  1:官网  2:辰阳(证券 sec_type = 1)  3:辰途(股权 sec_type = 2)
func HandleDBBySource(db *gorm.DB, source string) *gorm.DB {
	switch source {
	case "1":

	case "2":
		db = db.Where("tbl_securities.sec_type=1")
	case "3":
		db = db.Where("tbl_securities.sec_type=2")
	}

	return db
}
