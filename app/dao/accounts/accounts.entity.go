package accounts

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
)

type Accounts struct {
	AcctId        string `json:"acct_id,omitempty" gorm:"column:acct_id"`               // 账户ID
	Number        string `json:"number,omitempty" gorm:"column:number"`                 // 账号
	Name          string `json:"name,omitempty" gorm:"column:name"`                     // 名称
	AcctType      int    `json:"acct_type,omitempty" gorm:"column:acct_type"`           // 类型，见字典_AccountType
	InstitutionId string `json:"institution_id,omitempty" gorm:"column:institution_id"` // 开户机构ID，对应Contact表的ctc_id
	SubBranch     string `json:"sub_branch,omitempty" gorm:"column:sub_branch"`         // 支行/分支机构名称
	OwnerId       string `json:"owner_id,omitempty" gorm:"column:owner_id"`             // 所有人ID，对应Contact表的ctc_id
	Attribution   string `json:"attribution,omitempty" gorm:"column:attribution"`       // 归属地，用固话区号表示

	util.Model
}

func GetAccountsDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Accounts))
}

func (Accounts) TableName() string {
	return "tbl_account"
}
