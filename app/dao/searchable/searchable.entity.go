package searchable

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
	"time"
)

type Searchable struct {
	SrcId   string `json:"src_id" gorm:"column:src_id"`   // 账户ID
	Type    int    `json:"type" gorm:"column:type"`       // 账号
	Content string `json:"content" gorm:"column:content"` // 名称

	CreateTime time.Time `json:"create_time" gorm:"column:create_time"` // 创建时间
	LastUpdate time.Time `json:"last_update" gorm:"column:last_update"` // 最后一次更新时间
	Active     int       `json:"active" gorm:"column:active"`           // 0表示已删除，1表示有效;默认为1
}

func GetSearchableDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Searchable))
}

func (Searchable) TableName() string {
	return "tbl_searches"
}
