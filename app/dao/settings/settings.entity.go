package settings

import (
	"context"
	"gitee.com/zrj99123/common/app/dao/util"
	"gorm.io/gorm"
)

type Settings struct {
	UserId           string `json:"user_id" gorm:"column:user_id"`                     // 用户ID
	IncludePe        bool   `json:"include_pe" gorm:"column:include_pe"`               // 1 收益率计算/展示包含股权产品0 收益率计算/展示不包含股权产品
	AnnualizedReturn bool   `json:"annualized_return" gorm:"column:annualized_return"` // 1 年化收益率（收益率做年化处理）0 累计收益率（收益率不做年化处理）
}

func GetSettingsDB(ctx context.Context, defDB *gorm.DB) *gorm.DB {
	return util.GetDBWithModel(ctx, defDB, new(Settings))
}

func (Settings) TableName() string {
	return "tbl_settings"
}
