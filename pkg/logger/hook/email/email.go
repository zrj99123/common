package email

import (
	"fmt"
	"gopkg.in/gomail.v2"
	"regexp"
	"time"
)

type Sender struct {
	User     string
	Password string
	Host     string
	Port     int
	MailTo   []string
	Subject  string
	Content  string
}

type Receiver struct {
	Email string
}

func (r *Receiver) Check() bool {
	pattern := `\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*` //匹配电子邮箱
	reg := regexp.MustCompile(pattern)
	return reg.MatchString(r.Email)
}

func (s *Sender) clean() {

}

//检查 邮箱正确性
func (s *Sender) NewReceiver(email string, sender Sender) *Receiver {
	rec := &Receiver{Email: email}
	if rec.Check() {
		sender.MailTo = []string{email}
		return rec
	} else {
		fmt.Printf("email check fail 【%s】\n", email)
		return nil
	}
}
func (s *Sender) NewReceivers(receivers []*Receiver, sender *Sender) {
	for _, rec := range receivers {
		if rec.Check() {
			sender.MailTo = append(sender.MailTo, rec.Email)
		} else {
			fmt.Printf("email check fail 【%s】\n", rec.Email)
		}
	}
}

func SendEmail(receivers []*Receiver, sender Sender, subject, content string) error {
	sender.NewReceivers(receivers, &sender)
	sender.Subject = subject
	sender.Content = content

	e := gomail.NewMessage()
	e.SetHeader("From", e.FormatAddress(sender.User, "一诺错误监控"))
	e.SetHeader("To", sender.MailTo...)    //发送给多个用户
	e.SetHeader("Subject", sender.Subject) //设置邮件主题
	e.SetBody("text/html", sender.Content) //设置邮件正文
	d := gomail.NewDialer(sender.Host, sender.Port, sender.User, sender.Password)
	t := time.Now()
	err := d.DialAndSend(e)
	elapsed := time.Since(t)
	fmt.Println("邮件发送时长:", elapsed)
	if err != nil {
		fmt.Printf("error 邮件发送错误! %s  \n", err.Error())
		return err
	}
	return nil

}
