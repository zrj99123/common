package email

import (
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"strconv"
	"strings"
	"time"
)

type SubscribeMap map[logrus.Level][]*Receiver

type SubscribeHook struct {
	subMap SubscribeMap
	sender Sender
	db     *gorm.DB
}

//此处可以自实现hook 目前使用三方hook
func (h *SubscribeHook) Levels() []logrus.Level {
	return logrus.AllLevels
}

func Error(s string) bool {
	return strings.Contains(s, "不存在的路由！") || strings.Contains(s, "服务错误，请联系管理员！") ||
		strings.Contains(s, "绑定失败！") ||
		strings.Contains(s, "websocket 客户端连接失败！") || strings.Contains(s, "请求参数错误")
	//strings.Contains(s, "验证码发送失败")
}

func (h *SubscribeHook) Fire(entry *logrus.Entry) error {

	//h.Exec(entry)
	for level, receivers := range h.subMap {
		_level := level
		_receivers := receivers
		go func(level logrus.Level, receivers []*Receiver) {
			var (
				serverIp string
				clientIp string
				userID   string
				router   string
				method   string
				reason   string
				count    = 1
			)
			//命中 准备消费
			if level == entry.Level {
				if len(receivers) > 0 {
					time := entry.Time.String()
					_level := entry.Level.String()
					msg := entry.Message
					file := entry.Caller.File
					line := entry.Caller.Line
					file = file + ":" + strconv.Itoa(line)
					function := entry.Caller.Function
					//fmt.Println(time)
					//fmt.Println(_level)
					//fmt.Println(msg)
					//fmt.Println(function)
					//fmt.Println(file)
					splitMsg := strings.Split(msg, ",")
					if len(splitMsg) == 6 {
						serverIp = splitMsg[0]
						clientIp = splitMsg[1]
						userID = splitMsg[2]
						router = splitMsg[3]
						method = splitMsg[4]
						reason = splitMsg[5]
					} else {
						reason = msg
					}

					//if serverIp == "172.17.0.3" {
					//	serverIp = serverIp + "(!线上环境!)"
					//} else if serverIp == "172.17.0.14" {
					//	serverIp = serverIp + "(测试环境)"
					//} else {
					//	serverIp = serverIp + "(开发环境)"
					//}

					if Error(msg) {
						//SendEmail(receivers, h.sender, "[系统日志警报]", fmt.Sprintf("接口请求失败原因: %s", s))
						for {
							if count > 3 {
								break
							}
							err := SendEmail(receivers, h.sender, "[系统日志警报]", htmlStr(time, _level, function, file, serverIp, clientIp, userID, router, method, reason))
							if err == nil {
								break
							}
							count++
						}
					}

				}
			}
		}(_level, _receivers)
	}
	return nil
}

func NewSubscribeMap(level logrus.Level, receiverStr string) SubscribeMap {
	subMap := SubscribeMap{}
	addressList := strings.Split(receiverStr, ";")
	var receivers []*Receiver
	for _, address := range addressList {
		receivers = append(receivers, &Receiver{Email: address})
	}
	subMap[level] = receivers
	return subMap
}

/*func newSubScribeHook(subMap SubscribeMap,db *gorm.DB) *SubscribeHook {
	db.AutoMigrate(new(EmailLogger))

	return &SubscribeHook{
		subMap: subMap,
		db:     db,
	}
}
*/

// Create logger hook from gorm
func New(subMap SubscribeMap, sender Sender, db *gorm.DB) *SubscribeHook {
	//db.AutoMigrate(new(EmailLogger))

	return &SubscribeHook{
		subMap: subMap,
		sender: sender,
		db:     db,
	}
}

func (h *SubscribeHook) Exec(entry *logrus.Entry) error {
	//item := &EmailLogger{
	//	Level:     entry.Level.String(),
	//	Message:   entry.Message,
	//	CreatedAt: entry.Time,
	//}
	//
	////item.TraceID = "123"
	////item.UserID = 1
	////item.UserName = "cbx"
	////item.Tag = "log"
	//
	//data := entry.Data
	//
	//if len(data) > 0 {
	//	b, _ := json.Marshal(data)
	//	item.Data = string(b)
	//}

	return nil
}

func (h *SubscribeHook) Close() error {
	db, err := h.db.DB()
	if err != nil {
		return err
	}
	return db.Close()
}

type EmailLogger struct {
	ID        uint      `gorm:"primaryKey;"`     // id
	Level     string    `gorm:"size:20;index;"`  // 日志级别
	TraceID   string    `gorm:"size:128;index;"` // 跟踪ID
	UserID    uint64    `gorm:"index;"`          // 用户ID
	UserName  string    `gorm:"size:64;index;"`  // 用户名
	Tag       string    `gorm:"size:128;index;"` // Tag
	Message   string    `gorm:"size:1024;"`      // 消息
	Data      string    `gorm:"type:text;"`      // 日志数据(json)
	CreatedAt time.Time `gorm:"index"`           // 创建时间
}

func htmlStr(time string, level string, function string, file string, serverIp string, clientIp string, userId string, router string, method string, errorReason string) string {
	htmlStr := `<html>

<head>

</head>

<body>
    <div class="Section0" style="layout-grid:15.6000pt;">
        <p class="MsoNormal" style="line-height:150%;"><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">
                <font face="黑体">错误日志信息：</font>
            </span><span style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">
                <o:p></o:p>
            </span></p>
        <p class="MsoNormal" style="line-height:150%;"><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">
                <o:p>&nbsp;</o:p>
            </span></p>
        <p class="MsoNormal" style="line-height:150%;"><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;"></span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">` + serverIp + `</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">
                <o:p></o:p>
            </span></p>
        <p class="MsoNormal" style="line-height:150%;"><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;"></span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">` + clientIp + `</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">
                <o:p></o:p>
            </span></p>
        <p class="MsoNormal" style="line-height:150%;"><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">` + userId + `</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">
                <o:p></o:p>
            </span></p>
        <p class="MsoNormal" style="line-height:150%;"><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;"></span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">` + router + `</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">
                <o:p></o:p>
            </span></p>
        <p class="MsoNormal" style="line-height:150%;"><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">` + method + `</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">
                <o:p></o:p>
            </span></p>
        <p class="MsoNormal" style="line-height:150%;"><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">request_time</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">&nbsp;</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">=&nbsp;` + time + `</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">
                <o:p></o:p>
            </span></p>
        <p class="MsoNormal" style="line-height:150%;"><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">level</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">&nbsp;=&nbsp;` + level + `</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">
                <o:p></o:p>
            </span></p>
        <p class="MsoNormal" style="line-height:150%;"><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;"></span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">` + errorReason + `</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">
                <o:p></o:p>
            </span></p>
        <p class="MsoNormal" style="line-height:150%;"><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">func</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">&nbsp;</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">=&nbsp;` + function + `</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">
                <o:p></o:p>
            </span></p>
        <p class="MsoNormal" style="line-height:150%;"><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">file_path</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">&nbsp;</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">=&nbsp;` + file + `</span><span
                style="mso-spacerun:'yes';font-family:黑体;font-size:10.5000pt;mso-font-kerning:1.0000pt;">
                <o:p></o:p>
            </span></p>
    </div>
</body>

</html>`
	return htmlStr
}
