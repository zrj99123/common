package sms

import (
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/gogf/gf/encoding/gjson"
	"strings"
)

func SendSimpleModel(mobile, content string, params ...Param) error {
	if len(params) > 0 {
		url = params[0].URL
		uCode = params[0].Code
		uPass = params[0].Pass
	}

	body := strings.Replace(TemplateSms, "__user", uCode, -1)
	body = strings.Replace(body, "__pass", uPass, -1)
	body = strings.Replace(body, "__mobile", mobile, -1)
	body = strings.Replace(body, "__content", content, -1)
	client := resty.New()
	resp, err := client.R().
		SetHeader("Content-Type", "text/xml").
		SetBody(body).
		Post(url)
	if err != nil {
		return fmt.Errorf("yj post error,%+v", err)
	}

	if resp.StatusCode() != 200 {
		return fmt.Errorf("yj post error, status code is not 200[%d]", resp.StatusCode())
	}

	aJson, err := gjson.LoadXml(resp.String())
	if err != nil {
		return fmt.Errorf("loadXML error")
	}

	// 返回值， 出错则为负数[-3]， 正常为含时间戳的序号[220124]
	strRet := aJson.GetString("Envelope.Body.SendMsgResponse.SendMsgResult")
	if strRet == "" || strings.HasPrefix(strRet, "-") {
		return fmt.Errorf("发送短信失败")
	}

	return nil
}

func GetContentByTemplate(template string, msg []string) string {
	var (
		body = template
	)

	for _, v := range msg {
		body = strings.Replace(body, "__data", v, 1)
	}

	return body
}

var TemplateSms = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	<soap:Body>
		<SendMsg xmlns="http://tempuri.org/">
			<userCode>__user</userCode>
			<userPass>__pass</userPass>
			<DesNo>__mobile</DesNo>
			<Msg>__content</Msg>
			<autograph></autograph>
		</SendMsg>
	</soap:Body>
</soap:Envelope>`
