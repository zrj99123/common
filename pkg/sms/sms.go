package sms

import (
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/gogf/gf/encoding/gjson"
	"strings"
)

var (
	url   = "http://118.178.116.15/winnerrxd/api/ws/trigger"
	uCode = "GZXNYJ"
	uPass = "AZAvbAcd"
)

type Param struct {
	URL  string
	Code string
	Pass string
}

// 发送短信
func SendSms(mobile string, sn string, params ...Param) error {

	if len(params) > 0 {
		url = params[0].URL
		uCode = params[0].Code
		uPass = params[0].Pass
	}

	body := strings.Replace(tplSms, "__user", uCode, -1)
	body = strings.Replace(body, "__pass", uPass, -1)
	body = strings.Replace(body, "__mobile", mobile, -1)
	body = strings.Replace(body, "__sn", sn, -1)
	// utils.Debug(body)
	// utils.Debug(url)
	client := resty.New()
	resp, err := client.R().
		SetHeader("Content-Type", "text/xml").
		SetBody(body).
		Post(url)
	if err != nil {
		return fmt.Errorf("yj post error,%+v", err)
	}

	if resp.StatusCode() != 200 {
		return fmt.Errorf("yj post error, status code is not 200[%d]", resp.StatusCode())
	}

	aJson, err := gjson.LoadXml(resp.String())
	if err != nil {
		return fmt.Errorf("loadXML error")
	}

	// 返回值， 出错则为负数[-3]， 正常为含时间戳的序号[220124]
	// gfile.PutContents("d:/temp/yj_.xml", resp.String())
	strRet := aJson.GetString("Envelope.Body.SendMsgResponse.SendMsgResult")
	if strRet == "" || strings.HasPrefix(strRet, "-") {
		return fmt.Errorf("发送短信失败")
	}

	return nil
}

var tplSms = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	<soap:Body>
		<SendMsg xmlns="http://tempuri.org/">
			<userCode>__user</userCode>
			<userPass>__pass</userPass>
			<DesNo>__mobile</DesNo>
			<Msg>【谢诺投资】您的验证码为__sn，五分钟内有效，如非本人操作请忽略。</Msg>
			<autograph></autograph>
		</SendMsg>
	</soap:Body>
</soap:Envelope>`
