package sms

import (
	"log"
	"testing"
)

func TestGetContentByTemplate(t *testing.T) {
	data := GetContentByTemplate(
		"__data尊敬的__data：截止__data，您持有的__data产品当期产品净值为__data，累计净值为__data。如有疑问请咨询您的投资顾问或客服小诺:15914259306（微信同步）。",
		[]string{"【谢诺投资】", "test用户", "2022年1月1号", "saaaa", "1.11", "1.11"},
	)
	log.Println(data)
}

func TestSendSimpleModel(t *testing.T) {
	type args struct {
		mobile  string
		content string
		params  []Param
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := SendSimpleModel(tt.args.mobile, tt.args.content, tt.args.params...); (err != nil) != tt.wantErr {
				t.Errorf("SendSimpleModel() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
