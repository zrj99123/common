package jwtauth

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/zrj99123/common/pkg/cache"
	"gitee.com/zrj99123/common/pkg/errors"
	"gitee.com/zrj99123/common/pkg/util/crypto"
	"github.com/go-redis/redis"
	"github.com/golang-module/carbon/v2"
	"time"

	"github.com/dgrijalva/jwt-go"

	"gitee.com/zrj99123/common/pkg/auth"
)

const defaultKey = "cherami"

var (
	defaultOptions = options{
		tokenType:     "Bearer ",
		expired:       7200,
		signingMethod: jwt.SigningMethodHS512,
		signingKey:    []byte(defaultKey),
		keyfunc: func(t *jwt.Token) (interface{}, error) {
			if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, auth.ErrInvalidToken
			}
			return []byte(defaultKey), nil
		},
	}
	activeDuration = 5 * time.Minute
	//activeDuration = 1 * time.Minute

	tokenValueFormat = `{"datetime":"%s","unix":%d}`
)

type options struct {
	signingMethod jwt.SigningMethod
	signingKey    interface{}
	keyfunc       jwt.Keyfunc
	expired       int
	tokenType     string
}

// Option 定义参数项
type Option func(*options)

// SetSigningMethod 设定签名方式
func SetSigningMethod(method jwt.SigningMethod) Option {
	return func(o *options) {
		o.signingMethod = method
	}
}

// SetSigningKey 设定签名key
func SetSigningKey(key interface{}) Option {
	return func(o *options) {
		o.signingKey = key
	}
}

// SetKeyfunc 设定验证key的回调函数
func SetKeyfunc(keyFunc jwt.Keyfunc) Option {
	return func(o *options) {
		o.keyfunc = keyFunc
	}
}

// SetExpired 设定令牌过期时长(单位秒，默认7200)
func SetExpired(expired int) Option {
	return func(o *options) {
		o.expired = expired
	}
}

// New 创建认证实例
func New(store cache.Cacher, opts ...Option) *JWTAuth {
	o := defaultOptions
	for _, opt := range opts {
		opt(&o)
	}

	return &JWTAuth{
		opts:  &o,
		store: store,
	}
}

// JWTAuth jwt认证
type JWTAuth struct {
	opts  *options
	store cache.Cacher
}

type CustomClaims struct {
	Debug  bool   `json:"debug,omitempty"`
	RoleID string `json:"role,omitempty"`
	*jwt.StandardClaims
}

func (c CustomClaims) Valid() error {
	vErr := new(jwt.ValidationError)
	now := jwt.TimeFunc().Unix()

	// The claims below are optional, by default, so if they are set to the
	//// default value in Go, let's not fail the verification for them.
	//if c.VerifyExpiresAt(now, false) == false {
	//	delta := time.Unix(now, 0).Sub(time.Unix(c.ExpiresAt, 0))
	//	vErr.Inner = fmt.Errorf("token is expired by %v", delta)
	//	vErr.Errors |= jwt.ValidationErrorExpired
	//}

	if c.VerifyIssuedAt(now, false) == false {
		vErr.Inner = fmt.Errorf("Token used before issued")
		vErr.Errors |= jwt.ValidationErrorIssuedAt
	}

	if c.VerifyNotBefore(now, false) == false {
		vErr.Inner = fmt.Errorf("token is not valid yet")
		vErr.Errors |= jwt.ValidationErrorNotValidYet
	}

	if vErr.Errors == 0 {
		return nil
	}

	return vErr
}

// GenerateToken 生成令牌
func (a *JWTAuth) GenerateToken(ctx context.Context, userID, roleID, requestFrom string, expired int64) (auth.TokenInfo, error) {

	var (
		now       = time.Now()
		expiresAt = now.Add(time.Duration(a.opts.expired) * time.Second)
	)

	if expired != 0 {
		expiresAt = now.Add(time.Duration(expired) * time.Second)
	}

	token := jwt.NewWithClaims(a.opts.signingMethod, &CustomClaims{
		StandardClaims: &jwt.StandardClaims{
			IssuedAt:  now.Unix(),
			ExpiresAt: expiresAt.Unix(),
			NotBefore: now.Unix(),
			Subject:   userID,
			Audience:  requestFrom,
		},
		RoleID: roleID,
	})

	tokenString, err := token.SignedString(a.opts.signingKey)
	if err != nil {
		return nil, err
	}

	if err := a.callStore(func(store cache.Cacher) error {
		expired := time.Unix(expiresAt.Unix(), 0).Sub(now)
		tokenValue := fmt.Sprintf(tokenValueFormat, carbon.Time2Carbon(expiresAt).ToDateTimeString(), expiresAt.Unix())
		return store.Set(ctx, tokenString, tokenValue, expired)
	}); err != nil {
		return nil, err
	}

	tokenInfo := &tokenInfo{
		ExpiresAt:   expiresAt.Unix(),
		TokenType:   a.opts.tokenType,
		AccessToken: tokenString,
	}
	return tokenInfo, nil
}

// 解析令牌
func (a *JWTAuth) parseToken(tokenString string) (*CustomClaims, error) {
	token, err := jwt.ParseWithClaims(tokenString, &CustomClaims{}, a.opts.keyfunc)
	if err != nil || !token.Valid {
		return nil, errors.WrapTokenInvalidFailedResponse(fmt.Sprintf("%+v", err))
	}

	return token.Claims.(*CustomClaims), nil
}

func (a *JWTAuth) callStore(fn func(cache.Cacher) error) error {
	if store := a.store; store != nil {
		return fn(store)
	}
	return nil
}

// DestroyToken 销毁令牌
func (a *JWTAuth) DestroyToken(ctx context.Context, tokenString string) error {
	_, err := a.parseToken(tokenString)
	if err != nil {
		return err
	}

	// 如果设定了存储，将设定的令牌删除
	return a.callStore(func(store cache.Cacher) error {
		return store.Delete(ctx, tokenString)
	})
}

func (a *JWTAuth) AutomaticRenewal(ctx context.Context, tokenString string) error {

	now := time.Now()
	expiresAt := now.Add(time.Duration(a.opts.expired) * time.Second)

	if err := a.callStore(func(store cache.Cacher) error {
		expired := time.Unix(expiresAt.Unix(), 0).Sub(time.Now())
		tokenValue := fmt.Sprintf(tokenValueFormat, carbon.Time2Carbon(expiresAt).ToDateTimeString(), expiresAt.Unix())
		return store.Set(ctx, tokenString, tokenValue, expired)
	}); err != nil {
		return err
	}

	return nil
}

func (a *JWTAuth) ParseToken(ctx context.Context, tokenString string, m map[string]struct{}, f func(context.Context, string)) (map[string]string, error) {
	claims, err := a.parseToken(tokenString)
	if err != nil {
		return nil, err
	}

	var (
		now            = time.Now()
		expiresAt      = time.Unix(claims.ExpiresAt, 0)
		cacheExpiresAt = time.Time{}
		ret            = map[string]string{
			"userid": crypto.DecodeStringByURLEncoding(claims.Subject),
			"source": claims.Audience,
			"roleid": claims.RoleID,
		}
	)

	//存在
	if _, ok := m[crypto.DecodeStringByURLEncoding(claims.Subject)]; ok {
		return ret, nil
	}

	// 1. 第一次生成的 token 没有过期
	if now.Before(expiresAt) {

		// 1.1 没有过期，也不是 TokenExpiredAndActive 状态
		if now.After(expiresAt.Add(-activeDuration)) && now.Before(expiresAt) {
			// 1.1 没有过期，但是是 TokenExpiredAndActive 状态，这个时候，自动续期
			go f(ctx, tokenString)
		}
		//return ret, nil
	}

	// 2. 第一次生成的 token 过期了，检查 redis 中的 expired 是否过期
	if expiredValue, err := a.getCacheTokenExpired(ctx, tokenString); err != nil {
		return nil, err
	} else {
		cacheExpiresAt = time.Unix(expiredValue, 0)
	}

	if now.After(cacheExpiresAt) {
		return nil, errors.WrapTokenExpiredResponse(fmt.Sprintf(`{"now":"%v","expired":"%v"}`, now.Unix(), cacheExpiresAt.Unix()))
	}

	if now.After(cacheExpiresAt.Add(-activeDuration)) && now.Before(cacheExpiresAt) {
		go f(ctx, tokenString)
	}

	return ret, nil
}

func (a *JWTAuth) getCacheTokenExpired(ctx context.Context, originTokenString string) (int64, error) {

	var (
		err          error
		expiredValue string
	)
	if err = a.callStore(func(store cache.Cacher) error {
		expiredValue, err = store.Get(ctx, originTokenString)
		if err != nil {
			if err != redis.Nil {
				return errors.WrapTokenGetErrorResponse(fmt.Sprintf("%+v", err))
			} else {
				return errors.TokenExpired
			}
		}
		return nil
	}); err != nil {
		return 0, err
	}

	var kv map[string]interface{}
	_ = json.Unmarshal([]byte(expiredValue), &kv)

	v, ok := kv["unix"]
	if !ok {
		return 0, errors.WrapTokenGetErrorResponse(fmt.Sprintf("%+v", err))
	}

	if _, ok := v.(float64); !ok {
		return 0, errors.WrapTokenGetErrorResponse(fmt.Sprintf("%+v", err))
	}

	return int64(kv["unix"].(float64)), err
}

// Release 释放资源
func (a *JWTAuth) Release() error {
	return a.callStore(func(store cache.Cacher) error {
		return store.Close()
	})
}
