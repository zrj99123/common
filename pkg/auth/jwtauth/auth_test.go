package jwtauth

import (
	"context"
	"testing"

	"gitee.com/zrj99123/common/pkg/cache/buntdb"
	"github.com/stretchr/testify/assert"
)

func TestAuth(t *testing.T) {
	store, err := buntdb.NewStore(":memory:")
	assert.Nil(t, err)

	jwtAuth := New(store)

	defer jwtAuth.Release()

	ctx := context.Background()
	userID := "0681757b-5f92-42c2-a4cd-90976f50225f"
	roleID := "0681757b-5f92-42c2-a4cd-90976f50225e"
	token, err := jwtAuth.GenerateToken(ctx, userID, roleID, "1")
	assert.Nil(t, err)
	assert.NotNil(t, token)

	tokenUserID, err := jwtAuth.ParseUserID(ctx, token.GetAccessToken())
	assert.Nil(t, err)
	assert.Equal(t, userID, tokenUserID)

	err = jwtAuth.DestroyToken(ctx, token.GetAccessToken())
	assert.Nil(t, err)

	tokenUserID, err = jwtAuth.ParseUserID(ctx, token.GetAccessToken())
	assert.NotNil(t, err)
	assert.EqualError(t, err, "invalid token")
	assert.Empty(t, tokenUserID)
}
