package googleAuth

import (
	"fmt"
	"strings"
	"time"
)

func oneTimePassword(key []byte, data []byte) uint32 {
	hash := hmacSha1(key, data)
	offset := hash[len(hash)-1] & 0x0F
	hashParts := hash[offset : offset+4]
	hashParts[0] = hashParts[0] & 0x7F
	number := toUint32(hashParts)
	return number % 1000000
}

func MakeGoogleAuthenticator(secret string) (string, error) {
	secretUpper := strings.ToUpper(secret)
	secretKey, err := base32decode(secretUpper)
	if err != nil {
		return "", err
	}
	number := oneTimePassword(secretKey, toBytes(time.Now().Unix()/30))
	return fmt.Sprintf("%06d", number), nil
}

func GetQrcode(user, secret string) string {
	return fmt.Sprintf("otpauth://totp/%s?secret=%s", user, secret)
}

func GetQrcodeUrl(user, secret string) string {
	qrcode := GetQrcode(user, secret)
	return fmt.Sprintf("http://www.google.com/chart?chs=200x200&chld=M%%7C0&cht=qr&chl=%s", qrcode)
}

func VerifyCode(secret, code string) (bool, error) {
	_code, err := MakeGoogleAuthenticator(secret)
	if err != nil {
		return false, err
	}
	return _code == code, nil
}
