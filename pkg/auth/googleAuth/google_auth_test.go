package googleAuth

import (
	"fmt"
	"testing"
)

var codeTests = []struct {
	secret string
	code   int
}{
	{"3L57JZH4XRUH4ENCIYVFCJOZNEA7GX7C", 293240},
	{"3L57JZH4XRUH4ENCIYVFCJOZNEA7GX7C", 932068},
	{"3L57JZH4XRUH4ENCIYVFCJOZNEA7GX7C", 50548},
}

func TestComputeCode(t *testing.T) {
	for _, v := range codeTests {
		code, _ := MakeGoogleAuthenticator(v.secret)

		fmt.Printf("GetCode(%s): got %s expected %d\n", v.secret, code, v.code)
	}
}
