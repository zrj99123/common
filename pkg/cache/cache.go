package cache

import (
	"context"
	"time"
)

type Cacher interface {
	Set(ctx context.Context, tokenKey, tokenValue string, expiration time.Duration) error
	Get(ctx context.Context, key string) (string, error)
	Check(ctx context.Context, tokenKey string) error
	Delete(ctx context.Context, tokenKey string) error
	Close() error
	HSet(ctx context.Context, key, field string, value interface{}) error
}
