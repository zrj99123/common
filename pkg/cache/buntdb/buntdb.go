package buntdb

import (
	"context"
	"os"
	"path/filepath"
	"time"

	"github.com/tidwall/buntdb"
)

// NewStore 创建基于buntdb的文件存储
func NewStore(path string) (*Store, error) {
	if path != ":memory:" {
		os.MkdirAll(filepath.Dir(path), 0777)
	}

	db, err := buntdb.Open(path)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: db,
	}, nil
}

// Store buntdb存储
type Store struct {
	db *buntdb.DB
}

// Set ...
func (a *Store) Set(ctx context.Context, tokenKey, tokenValue string, expiration time.Duration) error {
	return a.db.Update(func(tx *buntdb.Tx) error {
		var opts *buntdb.SetOptions
		if expiration > 0 {
			opts = &buntdb.SetOptions{Expires: true, TTL: expiration}
		}
		_, _, err := tx.Set(tokenKey, tokenValue, opts)
		return err
	})
}

// Get ...
func (a *Store) Get(ctx context.Context, tokenKey string) (string, error) {
	var value string
	err := a.db.View(func(tx *buntdb.Tx) error {
		val, err := tx.Get(tokenKey)
		if err != nil && err != buntdb.ErrNotFound {
			return err
		}
		value = val
		return nil
	})
	return value, err
}

// Delete 删除键
func (a *Store) Delete(ctx context.Context, tokenKey string) error {
	return a.db.Update(func(tx *buntdb.Tx) error {
		_, err := tx.Delete(tokenKey)
		if err != nil && err != buntdb.ErrNotFound {
			return err
		}
		return nil
	})
}

// Check ...
func (a *Store) Check(ctx context.Context, tokenKey string) error {
	return a.db.View(func(tx *buntdb.Tx) error {
		_, err := tx.Get(tokenKey)
		if err != nil && err != buntdb.ErrNotFound {
			return err
		}
		return nil
	})
}

// Close ...
func (a *Store) Close() error {
	return a.db.Close()
}

func (s *Store) HSet(ctx context.Context, key, field string, value interface{}) error {
	return nil
}
