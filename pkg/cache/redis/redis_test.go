package redis

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	addr = "127.0.0.1:6379"
)

func TestStore(t *testing.T) {
	store := NewStore(&Config{
		Addr:      addr,
		DB:        1,
		KeyPrefix: "prefix",
	})

	defer store.Close()

	key := "test"
	ctx := context.Background()
	err := store.Set(ctx, key, "1", 0)
	assert.Nil(t, err)

	b := store.Check(ctx, key)
	assert.Equal(t, nil, b)

	b = store.Delete(ctx, key)
	assert.Equal(t, nil, b)
}
