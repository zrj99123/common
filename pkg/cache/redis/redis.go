package redis

import (
	"context"
	"fmt"
	"gitee.com/zrj99123/common/pkg/errors"
	"time"

	"github.com/go-redis/redis"
)

// Config redis配置参数
type Config struct {
	Addr      string // 地址(IP:Port)
	DB        int    // 数据库
	Password  string // 密码
	KeyPrefix string // 存储key的前缀
}

// NewStore 创建基于redis存储实例
func NewStore(cfg *Config) *Store {
	cli := redis.NewClient(&redis.Options{
		Addr:     cfg.Addr,
		DB:       cfg.DB,
		Password: cfg.Password,
	})
	return &Store{
		cli:    cli,
		prefix: cfg.KeyPrefix,
	}
}

// NewStoreWithClient 使用redis客户端创建存储实例
func NewStoreWithClient(cli *redis.Client, keyPrefix string) *Store {
	return &Store{
		cli:    cli,
		prefix: keyPrefix,
	}
}

// NewStoreWithClusterClient 使用redis集群客户端创建存储实例
func NewStoreWithClusterClient(cli *redis.ClusterClient, keyPrefix string) *Store {
	return &Store{
		cli:    cli,
		prefix: keyPrefix,
	}
}

type redisClienter interface {
	Get(key string) *redis.StringCmd
	Set(key string, value interface{}, expiration time.Duration) *redis.StatusCmd
	Expire(key string, expiration time.Duration) *redis.BoolCmd
	Exists(keys ...string) *redis.IntCmd
	TxPipeline() redis.Pipeliner
	Del(keys ...string) *redis.IntCmd
	Close() error
	HSet(key, field string, value interface{}) *redis.BoolCmd
}

// Store redis存储
type Store struct {
	cli    redisClienter
	prefix string
}

func (s *Store) wrapperKey(key string) string {
	return fmt.Sprintf("%s%s", s.prefix, key)
}

// Set ...
func (s *Store) Set(ctx context.Context, tokenKey, tokenValue string, expiration time.Duration) error {
	cmd := s.cli.Set(s.wrapperKey(tokenKey), tokenValue, expiration)
	return cmd.Err()
}

func (s *Store) Get(ctx context.Context, key string) (string, error) {

	val, err := s.cli.Get(s.wrapperKey(key)).Result()
	if err == redis.Nil {
		return "", err
	}

	return val, err
}

// Delete ...
func (s *Store) Delete(ctx context.Context, tokenKey string) error {
	cmd := s.cli.Del(s.wrapperKey(tokenKey))
	return cmd.Err()
}

// Check ...
func (s *Store) Check(ctx context.Context, tokenKey string) error {
	cmd := s.cli.Exists(s.wrapperKey(tokenKey))
	if cmd.Val() == 0 {
		return errors.TokenInvalid
	}
	return cmd.Err()
}

// Close ...
func (s *Store) Close() error {
	return s.cli.Close()
}

func (s *Store) HSet(ctx context.Context, key, field string, value interface{}) error {
	return s.cli.HSet(key, field, value).Err()
}
