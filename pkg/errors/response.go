package errors

import (
	"encoding/json"
	"fmt"
)

type ErrDetail struct {
	ERR    error  `json:"err,omitempty"`
	Detail string `json:"detail"`
}

// Response 定义响应错误
type Response struct {
	Code    int         `json:"code"`    // 错误码
	Message string      `json:"message"` // 错误消息
	Data    interface{} `json:"data"`
}

func (r *Response) String() string {
	res, _ := json.Marshal(r)
	return string(res)
}

func (r *Response) Error() string {

	if errDetail, ok := r.Data.(*ErrDetail); ok && errDetail.ERR != nil {

		return fmt.Sprintf("err:%v,detail:%v", errDetail.ERR.Error(), errDetail.Detail)
	}

	return r.Message
}

func UnWrapResponse(err error) *Response {

	if v, ok := err.(*Response); ok {
		return v
	}
	return nil
}

func NewResponse(code int, msg string, args ...interface{}) error {
	res := &Response{
		Code:    code,
		Message: msg,
		Data:    struct{}{},
	}
	if len(args) > 0 {
		res.Data = ErrDetail{Detail: fmt.Sprintf("%v", args...)}
	}
	return res
}

func wrapErrorResponse(respError *Response, err error, msg string, args ...interface{}) error {

	e := respError
	if e != nil {
		e.Data = &ErrDetail{
			Detail: fmt.Sprintf(msg, args...),
			ERR:    err,
		}
	}

	return e
}

func wrapErrorResponseWithTitleFormat(respError *Response, titleMsg []interface{}, detailErr error, detailMsg string, detailArgs ...interface{}) error {

	respErrorCopy := &Response{
		Code:    respError.Code,
		Message: respError.Message,
		Data:    respError.Data,
	}

	if respErrorCopy != nil {
		respErrorCopy.Data = &ErrDetail{
			Detail: fmt.Sprintf(detailMsg, detailArgs...),
			ERR:    detailErr,
		}
		if len(titleMsg) == 1 {
			respErrorCopy.Message = fmt.Sprintf(respErrorCopy.Message, titleMsg[0])
		}
		if len(titleMsg) == 2 {
			respErrorCopy.Message = fmt.Sprintf(respErrorCopy.Message, titleMsg[0], titleMsg[1])
		}
	}

	return respErrorCopy
}

func wrapSuccessResponse(resp *Response, args ...interface{}) error {

	var emptyJsonBody = map[string]interface{}{}

	// 1. 默认按照数据输出
	var iData interface{} = args

	// 2. 如果没有设置data参数,那么输出为{}空对象
	if len(args) == 0 {
		iData = emptyJsonBody
	}

	// 3. 如果data值是一个,那么输出为非空对象
	if len(args) == 1 {
		iData = args[0]
	}

	if resp != nil {
		resp.Data = iData
	}

	return resp
}

func WrapSuccessResponse(args ...interface{}) error {

	return wrapSuccessResponse(UnWrapResponse(Success), args...)
}

func WrapErrInternalServerResponse(msg string, args ...interface{}) error {

	return wrapErrorResponse(UnWrapResponse(ErrInternalServer), nil, msg, args...)
}

func WrapErrInternalServerResponseWithDetail(err error, msg string, args ...interface{}) error {

	return wrapErrorResponse(UnWrapResponse(ErrInternalServer), err, msg, args...)
}

func WrapBadRequestResponse(err error, msg string, args ...interface{}) error {

	return wrapErrorResponse(UnWrapResponse(ErrBadRequest), err, msg, args...)
}

func WrapMobileNumberLayoutErrorResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(MobileNumberLayoutError), nil, msg, args...)
}

func WrapMobileNumberNotExistErrorResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(MobileNumberNotExist), nil, msg, args...)
}

func WrapMobileNumberExistErrorResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(MobileNumberExists), nil, msg, args...)
}

func WrapVerifyCodeGenerationFailedResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(VerifyCodeGenerationFailed), nil, msg, args...)
}

func WrapVerifyCodeSendFailedResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(VerifyCodeSendFailed), nil, msg, args...)
}

func WrapVerifyCodeRequestTooMuchResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(VerifyCodeRequestTooMuch), nil, msg, args...)
}

func WrapAccountNotFoundResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(AccountNotFound), nil, msg, args...)
}

func WrapVerifyCodeGetFailedResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(VerifyCodeGetFailed), nil, msg, args...)
}

func WrapVerifyCodeNotMatchResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(VerifyCodeNotMatch), nil, msg, args...)
}

func WrapVerifyCodeTimeoutResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(VerifyCodeTimeout), nil, msg, args...)
}

func WrapWechatGetUnionIdFailedResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(WechatGetUnionIdFailed), nil, msg, args...)
}

func WrapWechatBindFailedResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(WechatBindFailed), nil, msg, args...)
}

func WrapWechatGetBoundInfoFailedResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(WechatGetBoundInfoFailed), nil, msg, args...)
}

func WrapWechatNotBindResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(WechatNotBound), nil, msg, args...)
}

func WrapTokenGenerationFailedResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(TokenGenerationFailed), nil, msg, args...)
}

func WrapRecordGetErrorResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(RecordGetError), nil, msg, args...)
}

func WrapClientAlreadyExistResponse(title []interface{}, msg string, args ...interface{}) error {
	return wrapErrorResponseWithTitleFormat(UnWrapResponse(ClientAlreadyExist), title, nil, msg, args...)
}

func WrapSecuritiesDeleteFailNotice1Response(title []interface{}, msg string, args ...interface{}) error {
	return wrapErrorResponseWithTitleFormat(UnWrapResponse(SecuritiesDeleteFailNotice1), title, nil, msg, args...)
}

func WrapInstitutionAlreadyExistResponse(title []interface{}, msg string, args ...interface{}) error {
	return wrapErrorResponseWithTitleFormat(UnWrapResponse(InstitutionAlreadyExist), title, nil, msg, args...)
}

func WrapSecuritiesAlreadyExistResponse(title []interface{}, msg string, args ...interface{}) error {
	return wrapErrorResponseWithTitleFormat(UnWrapResponse(SecuritiesAlreadyExist), title, nil, msg, args...)
}

func WrapSecuritiesNotExistResponse(title []interface{}, msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(SecuritiesNotExist), nil, msg, args...)
}

func WrapClientAndMobileAlreadyExistResponse(title []interface{}, msg string, args ...interface{}) error {
	return wrapErrorResponseWithTitleFormat(UnWrapResponse(ClientAndMobileAlreadyExist), title, nil, msg, args...)
}

func WrapTokenAutomaticRenewalFailedResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(TokenAutomaticRenewalFailed), nil, msg, args...)
}

func WrapUpDateRecordErrorResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(UpDateRecordError), nil, msg, args...)
}

func WrapTokenGetErrorResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(TokenGetError), nil, msg, args...)
}

func WrapTokenInvalidFailedResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(TokenInvalid), nil, msg, args...)
}

func WrapTokenEmptyResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(TokenEmpty), nil, msg, args...)
}

func WrapTokenExpiredResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(TokenExpired), nil, msg, args...)
}

func WrapRecordExistsResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(RecordExists), nil, msg, args...)
}

func WrapRecordNotExistsResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(RecordNotExists), nil, msg, args...)
}

func WrapInsertErrorResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(InsertRecordError), nil, msg, args...)
}

func WrapDelRecordErrorResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(DelRecordError), nil, msg, args...)
}

func WrapClientConnectFailedResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(ClientConnectFailed), nil, msg, args...)
}

func WrapFileUploadFailedResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(FileUploadFailed), nil, msg, args...)
}

func WrapFileDownFailedResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(FileDownFailed), nil, msg, args...)
}

func WrapIdentificationExistResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(IdentificationExist), nil, msg, args...)
}

func WrapProjectNameExistResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(InvestmentProjectNameExist), nil, msg, args...)
}

func WrapProjectDeleteFailNotice1Response(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(InvestmentProjectDeleteFailNotice1), nil, msg, args...)
}

func WrapProjectDeleteFailNotice2Response(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(InvestmentProjectDeleteFailNotice2), nil, msg, args...)
}

func WrapProjectTransferFailResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(TransferFail), nil, msg, args...)
}

func WrapProjectAddOwnershipRatioFailResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(AddOwnershipRatioFail), nil, msg, args...)
}

func WrapProjectSubOwnershipRatioFailResponse(msg string, args ...interface{}) error {
	return wrapErrorResponse(UnWrapResponse(SubOwnershipRatioFail), nil, msg, args...)
}
