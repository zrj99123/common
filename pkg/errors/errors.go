package errors

import (
	"github.com/pkg/errors"
)

// Define alias
var (
	New          = errors.New
	Errorf       = errors.Errorf
	Wrap         = errors.Wrap
	Wrapf        = errors.Wrapf
	WithStack    = errors.WithStack
	WithMessage  = errors.WithMessage
	WithMessagef = errors.WithMessagef
)

var (
	// ============== 后端定义错误 =================== //
	Success = NewResponse(200, "success")

	// 1. 请求路由相关
	ErrNotFound           = NewResponse(30100, "不存在的路由！", "no route found")
	ErrMethodNotAllow     = NewResponse(10103, "服务错误，请联系管理员！", "method not allowed")
	ErrTooManyRequests    = NewResponse(10102, "服务错误，请联系管理员！", "too many requests")
	ErrInternalServer     = NewResponse(10103, "服务错误，请联系管理员！", "app server error")
	NoPermissionForRouter = NewResponse(10104, "接口不可被访问", "router not allowed")

	// 2. token相关
	TokenInvalid                = NewResponse(20101, "Token 非法！", "token invalid")
	TokenEmpty                  = NewResponse(20102, "Token 为空！", "token empty")
	TokenExpired                = NewResponse(20103, "由于长时间没有操作，页面已超时，请重新登录！", "token expired")
	TokenGenerationFailed       = NewResponse(20104, "登录失败！", "token generate failed")
	TokenAutomaticRenewalFailed = NewResponse(20105, "服务错误，请联系管理员！", "token automatic renewal failed")
	TokenGetError               = NewResponse(20106, "Token 错误！", "token unknown error")

	// 3. 请求参数/路由相关
	ErrBadRequest = NewResponse(30100, "请求参数错误！", "bad request")

	// ============== 业务相关错误 =================== //
	// 4. 手机号相关错误
	MobileNumberIsBlank       = NewResponse(40100, "手机号未登记，请联系管理员！", "mobile number is blank")
	MobileNumberLayoutError   = NewResponse(40101, "手机号码格式有误", "mobile number layout error")
	MobileNumberNotExist      = NewResponse(40102, "手机号未登记，请联系管理员！", "mobile number not exist")
	MobileNumberAlreadyExist  = NewResponse(40103, "手机号未登记，请联系管理员！", "mobile number not exist")
	MobileNumberExists        = NewResponse(40104, "手机号已存在！", "mobile number already exists")
	MobileDefaultNumberExists = NewResponse(40105, "该手机号已关联其他联系人！", "mobile number already exists")

	// 5. 验证码相关错误
	VerifyCodeNotMatch         = NewResponse(50200, "验证码校验失败或已过期，请重新发送！", "verify code not match")
	VerifyCodeGenerationFailed = NewResponse(50201, "验证码发送失败！", "verify code generation failed")
	VerifyCodeSendFailed       = NewResponse(50202, "验证码发送失败！", "verify code send failed")
	VerifyCodeGetFailed        = NewResponse(50203, "验证码发送失败！", "verify code get failed")
	VerifyCodeTimeout          = NewResponse(50204, "验证码校验失败或已过期，请重新发送！", "verify code time out")
	VerifyCodeRequestTooMuch   = NewResponse(50205, "今日验证码已达上限，请联系管理员！", "verify code request too many")

	// 6. 账号相关错误
	AccountNotFound       = NewResponse(40102, "手机号未登记，请联系管理员！", "account not found")
	InvalidUUID           = NewResponse(40103, "非法UUID", "invalid uuid")
	RoleDeleteFailNotice1 = NewResponse(40104, "当前权限不可删除", "current role has not deleted rights")
	RoleUpdateFailNotice1 = NewResponse(40105, "当前权限不可更新", "current role has not update rights")
	RoleAddFailNotice1    = NewResponse(40106, "当前权限不可新增产品", "current role has not adding securities rights")
	RoleUpdateFileNotice1 = NewResponse(40107, "当前权限不可新增产品", "current role has not update files rights")
	RoleUpdateFileNotice2 = NewResponse(40108, "当前权限不可上传文件", "current role has not update files rights")
	RoleUpdateFileNotice3 = NewResponse(40111, "当前权限不可访问当前文件", "current role has not files rights")
	AccountAlreadyBound   = NewResponse(40112, "银行卡已绑定其他客户！", "account already bound")

	// 7. 微信接口相关
	WechatGetUnionIdFailed   = NewResponse(10103, "服务错误，请联系管理员！", "get wechat unionid failed")
	WechatBindFailed         = NewResponse(70301, "绑定失败！", "wechat bind failed")
	WechatAlreadyBound       = NewResponse(70302, "当前账户已被绑定，请联系管理员！", "wechat already bound")
	WechatNotBound           = NewResponse(70303, "当前账户未绑定，请联系管理员！", "wechat not bound")
	WechatGetBoundInfoFailed = NewResponse(10103, "服务错误，请联系管理员！", "wechat get bind info failed")

	// 8. 数据库相关
	RecordNotFound    = NewResponse(10103, "记录不存在", "record not found")
	RecordGetError    = NewResponse(10103, "服务错误，请联系管理员！", "record get error")
	UpDateRecordError = NewResponse(10103, "服务错误，请联系管理员！", "update record error")
	//RecordExists      = NewResponse(10103, "服务错误，请联系管理员！", "record already exists")
	RecordExists      = NewResponse(10103, "记录已存在！", "record already exists")
	RecordNotExists   = NewResponse(10103, "服务错误，请联系管理员！", "record not exists")
	InsertRecordError = NewResponse(10103, "服务错误，请联系管理员！", "insert record error")
	DelRecordError    = NewResponse(10103, "服务错误，请联系管理员！", "delete record error")

	// 9. 客户模块
	PrimaryServicePersonnelExists = NewResponse(90302, "主服务人员已经存在！", "primary service personnel exists")
	ClientAlreadyExist            = NewResponse(90303, " (%s)已存在", "client already exists")
	ClientAndMobileAlreadyExist   = NewResponse(90304, "（%s）手机号与（%s*）重复，请确认", "client's name and mobile has repeated")
	InstitutionAlreadyExist       = NewResponse(90305, " (%s）已存在", "institution already exists")
	SecuritiesDeleteFailNotice1   = NewResponse(90307, "有关联文件（%s）未处理！", "can not be deleted")
	SecuritiesDeleteFailNotice2   = NewResponse(90308, "有关联交易订单，不可删除！", "can not be deleted")
	SecuritiesDeleteFailNotice3   = NewResponse(90309, "有关联成交交易，不可删除！", "can not be deleted")
	//TradeOrderDeleteFailNotice1   = NewResponse(90310, "该笔交易已经属于\"已持有\"状态不能删除！", "can not be deleted")
	TradeOrderDeleteFailNotice1       = NewResponse(90310, "该订单已确认，无法删除", "can not be deleted")
	TradeOrderUpdateFailNotice2       = NewResponse(90311, "状态为已打款，不可更改", "status can't be changed")
	TradeOrderUpdateFailNotice3       = NewResponse(90312, "状态为已打款，不可更改", "不可删除")
	AddInvestmentAdvisorRefuse        = NewResponse(90313, "投顾只允许设置一个且必须设置一个！", "investment advisor only one")
	AddReferrerPersonnelRefuse        = NewResponse(90314, "引荐人只允许唯一！", "referrer personnel exists")
	TradeOrderDeleteFailNotice4       = NewResponse(90315, "该订单已确认，无法修改", "can not be update")
	NoPermissionForTeam               = NewResponse(90316, "股权团队不可访问客户", "team has no permission for accessing client list")
	TradeOrderDeleteFailNotice5       = NewResponse(90317, "转让人持有产品的份额为0,无法转让!", "can not be transfer")
	NoPermissionForUpdateOrganization = NewResponse(90318, "投顾不可修改归属", "has no permission for change client organization")
	NoPermissionForPersonal           = NewResponse(90319, "该客户不在您的可见范围", "has no permission for this client's info")
	NoPermissionForRole6Notice1       = NewResponse(90320, "产品中心角色不可访问TA交易", "has no permission for transfer agent")
	NoPermissionForRole6Notice2       = NewResponse(90321, "产品中心角色不可访问产品持有人", "has no permission for securities's holders")
	NoPermissionForRole6Notice3       = NewResponse(90322, "产品中心角色不可访问持仓项目", "has no permission for role")
	NoPermissionForRole6Notice4       = NewResponse(90323, "产品中心角色不可访问客户", "has no permission for role")
	TradeOrderUpdateFailNotice4       = NewResponse(90324, "投顾不可修改佣金与业绩分配内容", "can not be updated")
	TradeOrderAddFailNotice1          = NewResponse(90325, "同一天不能添加两笔相同的交易订单", "can not be added")
	NoPermissionForDocument           = NewResponse(90326, "当前权限不可操作文档", "current permission has no rights for documents")
	TradeOrderUpdateFailNotice5       = NewResponse(90327, "请完善打款金额!", "order amount is null")
	TradeOrderUpdateFailNotice6       = NewResponse(90328, "请户完善打款日期!", "order trade_day is null")

	// 10. 文件模块
	File1MSizeExceeds    = NewResponse(11101, "文件大小超出1M", "file size exceeds 1M")
	FileSizeExceeds      = NewResponse(11101, "图片大小超出2M", "file size exceeds 2M")
	OtherFileSizeExceeds = NewResponse(11101, "文件大小超出20M", "file size exceeds 20M")
	FileUploadFailed     = NewResponse(11102, "文件上传失败！", "file upload failed")
	FileDownFailed       = NewResponse(11103, "文件下载失败！", "file download failed")

	// 11. Websocket
	ClientConnectFailed = NewResponse(12101, "websocket 客户端连接失败！", "websocket client connect failed")

	// 12. 证件相关
	IdentificationExist              = NewResponse(13101, "证件已经存在", "identification already exists")
	IdentificationAlreadyBound       = NewResponse(13102, "证件已被其他客户绑定", "identification already bound")
	ClientAlreadyBoundIdentification = NewResponse(13103, "客户已存在该类型证件", "identification already bound")

	// 14. 项目相关
	InvestmentProjectNameExist         = NewResponse(14101, "项目名称已经存在", "project's name already exists")
	InvestmentProjectDeleteFailNotice1 = NewResponse(14102, "有关联订单,不可删除!", "can not be deleted")
	InvestmentProjectDeleteFailNotice2 = NewResponse(14103, "在当前或其他跟进状态中有关联文件,不可删除!", "can not be deleted")
	TransferFail                       = NewResponse(14104, "转让失败!", "can not be transfer")
	AddOwnershipRatioFail              = NewResponse(14105, "增资失败!目标占比小于当前占比", "can not be add ratio")
	SubOwnershipRatioFail              = NewResponse(14106, "减持失败!减少占比大于当前占比", "can not be sub ratio")
	AmountFail                         = NewResponse(14107, "金额错误!", "can not be sub ratio")
	NetAmountFail                      = NewResponse(14107, "投资底层标金额必须小于实缴金额!", "can not be sub ratio")
	ChargeFail                         = NewResponse(14108, "计提业绩报酬金额必须小于可分配金额!", "can not be sub ratio")
	VATFail                            = NewResponse(14109, "增值税必须小于可分配金额!", "can not be sub ratio")
	IITFail                            = NewResponse(14110, "个人所得税小于可分配金额!", "can not be sub ratio")
	TradeOrderExist                    = NewResponse(14111, "分红订单已存在!", "can not be sub ratio")

	// 15. 标签库相关
	AddTagFail               = NewResponse(15001, "已有该标签或身份,不可重复添加同样的标签或身份!", "can not add repeat tag")
	UpdateSocialRelationFail = NewResponse(15002, "已是该身份,请勿重复修改为同一值!", "can not update to same value ")

	// 16. 产品相关
	SecHasNotOpenDay       = NewResponse(16001, "非证券产品没有开放日", "securities has not open days ")
	SecuritiesAlreadyExist = NewResponse(16002, " (%s）已存在", "securities already exists")
	SecuritiesNotExist     = NewResponse(16003, "产品不存在", "securities don`t exists")
	BenchmarkNotExist      = NewResponse(16004, "基准不存在", "securities benchmark don`t exists")

	// 17. 工作流相关
	WorkFlowNameExist = NewResponse(17101, "工作流名称已经存在", "workflow's name already exists")
)
