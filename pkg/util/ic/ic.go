package ic

import (
	"errors"
	"gitee.com/zrj99123/common/pkg/util/crypto"
	"github.com/guanguans/id-validator"
)

func GetBirthdayByICNumber(defaultIc string) (int64, error) {
	var (
		ic   string
		info idvalidator.IdInfo
		err  error
	)

	if defaultIc != "" {

		decodedIC := crypto.DecodeStringByURLEncoding(defaultIc)

		if idvalidator.IsValid(decodedIC, false) {
			ic = decodedIC
		} else {
			if idvalidator.IsValid(defaultIc, false) {
				ic = defaultIc
			} else {
				return 0, errors.New("it is not ic")
			}
		}

		info, err = idvalidator.GetInfo(ic, false)
		if err != nil {
			return 0, err
		}
	}

	return info.Birthday.Unix(), nil
}
