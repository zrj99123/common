package ic

import (
	"fmt"
	"gitee.com/zrj99123/common/pkg/util/time"
	"testing"
)

func TestGetBirthdayByICNumber(t *testing.T) {

	datas := []string{
		//"Nvj8iw8KGbNh2A6PvrNnNKMRipnoZ_97jHaw_anFR6k=",
		//"acHvjw5HrHWm1BdMrVdtwUpDdek7XmoVq2iQ1xXqtxQ=",
		//"YbEX0tPy1bZZwyCsmkaM9aPkv07nP32aL8BDrb0_bVQ=",
		//"Pn5F2wwqf8LPJgVAEFBjsJJdneBMcKC8ujZMFzdBetU=",
		//"mpBJ1XjDTQ_oyGaBeak_htLOmKKzlBaeT0lKQPHkhYU=",
		//"dAMfCA-8QyTC-T7fNNIC7EGa7Q3HRN0mSdI5xe_40Ck=",
		//"yC49PNpGMdJF2zA09YXQvg==",
		//"8f1OlyoQxctKjXmTf8bxh09r5AZfShGhsuSWnaHeKv0=",
		//"eG5_7nyYatjiTZBlrKoASub-Y6k1kej5dDFNBaO1Rxg=",
		//"gNF7qlrqU3DJZwrNlPAJZuXXx9yBj1IRZzACx_Ihz0Q=",
		//"4Im6Aa8iCS-7SWcZtImeCdw-nATd8LU04hoOPxU50W8=",
		//"vbad2VCPVg5kHXcEO9bdAcqvFw_LruwMmCkivjuml0k=",
		//"p0XZBy10gg-fayZNAh5f8xhMl0hykYN-ZJzg38NZyuQ=",
		//"Ipqf922xv5OtBHOHu-FeYA==",
		//"512501197409182733",
		//"1345645768536",
		//"HACFKAYdjCO31VKOrLsM0SF1Bc_vwvl7TfFcKnYyEGY=",
		//"8087799",
		//"6rlDOUBbzKU-U-Emh819zg==",
		"BhL0vZVnQCnhw34HHQCBylCY7WbCTJr2vtAtllWDVxs=",
	}

	for _, data := range datas {
		number, err := GetBirthdayByICNumber(data)
		//if err != nil {
		//	fmt.Println(err)
		//}
		fmt.Println(data, "------->", time.Format(number), "err:", err)
	}

}
