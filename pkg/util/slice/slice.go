package slice

import (
	"github.com/scylladb/go-set/iset"
	"github.com/scylladb/go-set/strset"
)

// 取两个切片的交集
func Intersect(a []string, b []string) []string {

	setA, setB := strset.New(a...), strset.New(b...)
	setIntersection := strset.Intersection(setA, setB)

	return setIntersection.List()
}

// 取两个切片的交集
func IntersectInt(a []int, b []int) []int {

	setA, setB := iset.New(a...), iset.New(b...)
	setIntersection := iset.Intersection(setA, setB)

	return setIntersection.List()
}

// 取两个切片的差集
func Difference(a []string, b []string) []string {

	setA, setB := strset.New(a...), strset.New(b...)
	setIntersection := strset.Difference(setA, setB)

	return setIntersection.List()
}

// 取两个切片的两两差集后的并集
func SymmetricDifference(a []string, b []string) []string {

	setA, setB := strset.New(a...), strset.New(b...)
	setIntersection := strset.SymmetricDifference(setA, setB)

	return setIntersection.List()
}
