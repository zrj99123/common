package common

import (
	"fmt"
	"os"
)

//得到项目运行路径
func GetAppPath() (string, error) {
	path, err := os.Getwd()
	if err != nil {
		return "", err
	}
	return path, nil
}

// 获取号码加星
func GetNumberxxx(number string) (t string) {

	if number == "" {
		return
	}

	// ENUO-2523 加星处理
	length := len(number) - 4
	if length < 0 {
		length = 0
	}

	number = number[length:]

	t = fmt.Sprintf("***********%v", number)

	return
}

//删除重复string
func DelDuplicateString(slice []string) []string {
	var (
		m   = map[string]struct{}{}
		res = []string{}
	)

	for _, v := range slice {
		m[v] = struct{}{}
	}

	for key := range m {
		res = append(res, key)
	}

	return res
}

func GetFirstName(name string) string {
	return string([]rune(name)[1:])
}

func GetLastName(name string) string {
	return string([]rune(name)[:1])
}
