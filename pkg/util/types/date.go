package types

import (
	"database/sql/driver"
	"fmt"
	"github.com/golang-module/carbon/v2"
	"time"
)

type Date string

func (t Date) Value() (driver.Value, error) {

	return t, nil
}

func (t *Date) Scan(v interface{}) error {
	value, ok := v.(time.Time)
	if ok {
		*t = Date(carbon.Time2Carbon(value).ToDateString())
		return nil
	}

	return fmt.Errorf("can not convert %v to date", v)
}
