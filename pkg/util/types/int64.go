package types

import (
	"database/sql/driver"
	"fmt"
	"time"
)

type Int64 int64

func (t Int64) Value() (driver.Value, error) {

	return t, nil
}

func (t *Int64) Scan(v interface{}) error {
	value, ok := v.(time.Time)
	if ok {
		*t = Int64(value.Unix())
		return nil
	}

	return fmt.Errorf("can not convert %v to timestamp", v)
}
