package time

import (
	"fmt"
	"github.com/6tail/lunar-go/HolidayUtil"
	"github.com/golang-module/carbon/v2"
	"math"
	"strconv"
	"time"
)

// duration 单位是天
func Duration2YMD(duration int) string {

	var ymdString string
	year := duration / 365
	if year != 0 {
		ymdString = fmt.Sprintf("%v年", year)
	}
	month := (duration - year*365) / 30
	if month != 0 {
		ymdString += fmt.Sprintf("%v月", month)
	}
	day := duration - year*365 - month*30
	if day != 0 {
		ymdString += fmt.Sprintf("%v天", day)
	}

	return ymdString
}

func GetNowTime() *time.Time {
	t, _ := time.ParseInLocation("2006-01-02 15:04:05", time.Now().Format("2006-01-02 15:04:05"), time.Local)
	return &t
}

func GetNowDateTime() *time.Time {
	//t, _ := time.ParseInLocation("2006-01-02", time.Now().Format("2006-01-02"), time.Local)\
	t, _ := time.Parse("2006-01-02", time.Now().Format("2006-01-02"))
	return &t
}

//返回值YYYY-MM-DD
func TimeToDateString(t time.Time) string {
	return t.Format("2006-01-02")
}

//返回值YYYY/MM/DD
func TimeToDateTiltString(t time.Time) string {
	return t.Format("2006/01/02")
}

//返回值YYYY/MM/DD
func GetMonth(t time.Time) string {
	return t.Format("01")
}

//返回值YYYY-MM
func TimeToYMString(t time.Time) string {
	return t.Format("2006-01")
}

//获取每年最后一天
func GetYearLastDay(date time.Time) *time.Time {
	loc, _ := time.LoadLocation("Local")
	theTime, _ := time.ParseInLocation("2006-01-02", date.Format("2006")+"-01-01", loc)
	t2 := theTime.AddDate(1, 0, -1)
	return &t2
}

func GetMonthLastDay(times string) *time.Time {
	timeLayout := "2006-01-02"
	loc, _ := time.LoadLocation("Local")
	theTime, _ := time.ParseInLocation(timeLayout, times+"-01", loc)
	t2 := theTime.AddDate(0, 1, -1)
	return &t2
}

//获取每月最后一个工作日(times YYYY-MM-DD)
func GetMonthLastWorkDay(date time.Time) *time.Time {
	t := *GetMonthLastDay(TimeToYMString(date))

	for IsWeekEndOrHoliday(t) {
		t = t.AddDate(0, 0, -1)
	}

	return &t
}

func GetMonthFirstDay(times string) *time.Time {
	timeLayout := "2006-01-02"
	loc, _ := time.LoadLocation("Local")
	theTime, _ := time.ParseInLocation(timeLayout, times+"-01", loc)
	return &theTime
}

func DateYMDStringToTime(times string) (*time.Time, error) {
	timeLayout := "2006-01-02"
	loc, _ := time.LoadLocation("Local")
	theTime, err := time.ParseInLocation(timeLayout, times, loc)
	if err != nil {
		return nil, err
	}
	return &theTime, nil
}

//获得指定日期 季度的初始和结束日期
func GetQuarterDate(dateTime time.Time) (time.Time, time.Time) {
	timeLayout := "2006-01-02 15:04:05"
	loc, _ := time.LoadLocation("Local")

	year := dateTime.Format("2006")
	month := int(dateTime.Month())
	var firstOfQuarter string
	var lastOfQuarter string
	if month >= 1 && month <= 3 {
		firstOfQuarter = year + "-01-01 00:00:00"
		lastOfQuarter = year + "-03-31 23:59:59"
	} else if month >= 4 && month <= 6 {
		firstOfQuarter = year + "-04-01 00:00:00"
		lastOfQuarter = year + "-06-30 23:59:59"
	} else if month >= 7 && month <= 9 {
		firstOfQuarter = year + "-07-01 00:00:00"
		lastOfQuarter = year + "-09-30 23:59:59"
	} else {
		firstOfQuarter = year + "-10-01 00:00:00"
		lastOfQuarter = year + "-12-31 23:59:59"
	}

	strTime, _ := time.ParseInLocation(timeLayout, firstOfQuarter, loc)
	endTime, _ := time.ParseInLocation(timeLayout, lastOfQuarter, loc)
	return strTime, endTime
}

//获取之前日期最近的星期一 (两周之前 14天)
func GetLastMonday(dateTime time.Time) time.Time {
	for dateTime.Weekday() != time.Monday {
		dateTime = dateTime.AddDate(0, 0, -1)
	}

	return dateTime
}

//获取之前日期最近的星期五 (两周之前 14天)
//tmp 当前日期往前几次遍历的周五
func GetLastlyFriday(dateTime time.Time, tmp int) time.Time {
	var (
		flag int
	)

	day := carbon.Parse(dateTime.Format("2006-01-02"))
	for flag != tmp {
		day = day.AddDays(-1)
		if day.IsFriday() {
			flag++
		}
	}

	return day.Carbon2Time()
}

//获取之前日期最近的星期五 (两周之前 14天)
//tmp 当前日期往前几次遍历的周五
func GetNearFriday(dateTime time.Time) time.Time {
	for dateTime.Weekday() != time.Friday {
		dateTime = dateTime.AddDate(0, 0, 1)
	}

	return dateTime
}

//判断是否周末或节假日
func IsWeekEndOrHoliday(date time.Time) bool {
	if date.Weekday() == time.Saturday || date.Weekday() == time.Sunday {
		return true
	}
	dateYMDStr := TimeToDateString(date)

	holiday := HolidayUtil.GetHoliday(dateYMDStr)
	if holiday != nil && !holiday.IsWork() {
		return true
	}

	return false
}

//是否是每周最后一个工作日
func IsWeekLastWorkingDay(date time.Time) bool {
	var (
		strWeekDate = carbon.Parse(TimeToDateString(date)).StartOfWeek().AddDays(1)
		endWeekDate = carbon.Parse(TimeToDateString(date)).EndOfWeek().AddDays(1)
		lastWorking string
	)

	//寻找每周最后一个工作日
	for endWeekDate.Gte(strWeekDate) {
		if !IsWeekEndOrHoliday(endWeekDate.Carbon2Time()) {
			lastWorking = endWeekDate.ToDateString()
			break
		}
		endWeekDate = endWeekDate.AddDays(-1)
	}

	if TimeToDateString(date) == lastWorking {
		return true
	}

	return false
}

//是否每月最后一个工作日
func IsMonthLastWorkingDay(date time.Time) bool {
	var (
		strWeekDate = carbon.Parse(TimeToDateString(date)).StartOfMonth()
		endWeekDate = carbon.Parse(TimeToDateString(date)).EndOfMonth()
		lastWorking string
	)

	//寻找每周最后一个工作日
	for endWeekDate.Gte(strWeekDate) {
		if !IsWeekEndOrHoliday(endWeekDate.Carbon2Time()) {
			lastWorking = endWeekDate.ToDateString()
			break
		}
		endWeekDate = endWeekDate.AddDays(-1)
	}

	if TimeToDateString(date) == lastWorking {
		return true
	}

	return false
}

//date往后最近一个工作日
func GetNearWorkDay(date time.Time) time.Time {
	for IsWeekEndOrHoliday(date) {
		date = date.AddDate(0, 0, 1)
	}

	return date
}

//date往前最近一个工作日
func GetLastWorkDay(date time.Time) time.Time {
	for IsWeekEndOrHoliday(date) {
		date = date.AddDate(0, 0, -1)
	}

	return date
}

func TimeToYYYYMMDD(t *time.Time) string {
	return t.Format("20060102")
}

func ExcelDateToDate(excelDate string) *time.Time {
	excelTime := time.Date(1899, time.December, 30, 0, 0, 0, 0, time.UTC)
	var days, _ = strconv.Atoi(excelDate)
	value := excelTime.Add(time.Second * time.Duration(days*86400))
	return &value
}

func GetNowTimeStr() string {
	return time.Now().Format("2006-01-02 15:04:05")
}

func GetNowDateStr() string {
	return time.Now().Format("2006-01-02")
}

func GetYesterdayDateStr() string {
	return time.Now().AddDate(0, 0, -1).Format("2006-01-02")
}

//两个日期之间相差天数
func Days(strTimestamp, endTimestamp int64) int {
	var midnightUnix = func(t time.Time) int64 {
		y, m, d := t.Date()
		return time.Date(y, m, d+1, 0, 0, 0, 0, time.Local).Unix()
	}

	var days = 0
	for {
		if midnightUnix(time.Unix(strTimestamp, 0).AddDate(0, 0, days)) >= endTimestamp {
			days++
			break
		}
		days++
	}
	return days
}

//超过本周周五 获取上周五 未超过获取上上周周五
//yyyy-mm-dd
func GetGrowthChartEndDay(date string) time.Time {
	day := carbon.Parse(date)

	//是否超过周五
	if day.Carbon2Time().Weekday() > time.Friday {
		return GetLastlyFriday(day.Carbon2Time(), 2)
	}

	return GetLastlyFriday(day.Carbon2Time(), 2)
}

//两个日期之间年份相差的年数
func DiffAbsInYears(day1, day2 carbon.Carbon) int {
	tmp := float64(day1.Year() - day2.Year())

	return int(math.Abs(tmp))
}
