package time

import (
	"fmt"
	"github.com/golang-module/carbon/v2"
	"testing"
	"time"
)

func TestDuration2YMD(t *testing.T) {

	items := []struct {
		duration int
		expect   string
	}{
		{
			366,
			"1年1天",
		},
		{
			400,
			"1年1月5天",
		},
		{
			32,
			"1月2天",
		},
		{
			2,
			"2天",
		},
		{
			30,
			"1月",
		},
		{
			60,
			"2月",
		},
		{
			361,
			"12月1天",
		},
	}

	for _, item := range items {
		if got := Duration2YMD(item.duration); got != item.expect {
			t.Errorf("error got != item.expect got is %v expect is %v duration is :%v \n", got, item.expect, item.duration)
		} else {
			fmt.Println("got result:", got, " duration is:", item.duration)
		}
	}
}

func TestGetLastlyFriday(t *testing.T) {
	test := GetLastlyFriday(time.Now(), 2)

	fmt.Println(test)
}

func TestGetMonthLastWorkDay(t *testing.T) {
	t1, _ := DateYMDStringToTime("2022-02-02")
	fmt.Println("got result:", t1, " duration is:", GetMonthLastWorkDay(*t1))

	t2, _ := DateYMDStringToTime("2022-12-02")
	fmt.Println("got result:", t1, " duration is:", GetMonthLastWorkDay(*t2))

	t3, _ := DateYMDStringToTime("2022-04-30")
	fmt.Println("got result:", t1, " duration is:", GetMonthLastWorkDay(*t3))
}

func TestGetNearFriday(t *testing.T) {
	t1, _ := DateYMDStringToTime("2022-02-04")

	fmt.Println("got result:", t1, " duration is:", GetNearFriday(*t1))
}

func TestGetLastMonday(t *testing.T) {
	t1, _ := DateYMDStringToTime("2022-12-07")

	fmt.Println("got result:", t1, " duration is:", GetLastMonday(*t1))
}

func TestGetNearWorkDay(t *testing.T) {
	t1, _ := DateYMDStringToTime("2022-12-07")

	fmt.Println("got result:", t1, " duration is:", GetNearWorkDay(*t1))

	t2, _ := DateYMDStringToTime("2022-12-31")

	fmt.Println("got result:", t2, " duration is:", GetNearWorkDay(*t2))
}

func TestIsWeekEndOrHoliday(t *testing.T) {
	t2, _ := DateYMDStringToTime("2022-10-07")

	fmt.Println("got result:", t2, " duration is:", IsWeekEndOrHoliday(*t2))

	t3, _ := DateYMDStringToTime("2023-01-02")

	fmt.Println("got result:", t3, " duration is:", IsWeekEndOrHoliday(*t3))

}

func TestGetYearLastDay(t *testing.T) {
	t2, _ := DateYMDStringToTime("2022-12-02")

	fmt.Println("got result:", t2, " duration is:", GetYearLastDay(*t2))
}

func TestGetLastWorkDay(t *testing.T) {
	t2, _ := DateYMDStringToTime("2022-12-31")

	fmt.Println("got result:", t2, " duration is:", GetLastWorkDay(*t2))
}

func TestGetGrowthChartEndDay(t *testing.T) {
	datas := []string{
		"2023-01-05",
		"2023-01-06",
		"2023-01-07",
	}

	for _, data := range datas {
		fmt.Println("aes before:", data, " aes after", GetGrowthChartEndDay(data))
	}
}

func TestIsLastWeekWorkingDay(t *testing.T) {
	datas := []carbon.Carbon{
		carbon.Parse("2022-10-03"),
		carbon.Parse("2022-10-08"),
		carbon.Parse("2022-10-09"),
		carbon.Parse("2023-02-10"),
	}

	for _, data := range datas {
		fmt.Println("aes before:", data, " aes after", IsWeekLastWorkingDay(data.Carbon2Time()))
	}

}

func TestIsMonthLastWorkingDay(t *testing.T) {
	datas := []carbon.Carbon{
		carbon.Parse("2023-01-29"),
		carbon.Parse("2023-01-31"),
		carbon.Parse("2022-12-29"),
		carbon.Parse("2022-12-30"),
		carbon.Parse("2022-12-31"),
	}

	for _, data := range datas {
		fmt.Println("aes before:", data, " aes after", IsMonthLastWorkingDay(data.Carbon2Time()))
	}

}
