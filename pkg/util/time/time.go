package time

import "time"

var (
	timeFormat = "2006-01-02 15:04:05.999999"
)

func ToUnix(value string, layout ...string) int64 {

	if len(layout) > 0 {
		timeFormat = layout[0]
	}

	tt, _ := time.Parse(timeFormat, value)

	return tt.Unix()
}

func Format(sec int64, layout ...string) string {

	if len(layout) > 0 {
		timeFormat = layout[0]
	}

	return time.Unix(sec, 0).Format(timeFormat)
}
