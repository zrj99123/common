package time

import "github.com/golang-module/carbon/v2"

//获取每周 以及每月最后一个工作日
//每周 tag:2
//每月 tag:4
func GetWeekAndMonthLastWorkingDay(strDate carbon.Carbon) []map[string]interface{} {
	var (
		m   = []map[string]interface{}{}
		now = carbon.Parse(GetNowDateStr())
	)

	for strDate.Lte(now) {
		var tag int
		if IsWeekLastWorkingDay(strDate.Carbon2Time()) {
			tag += 2
		}
		if IsMonthLastWorkingDay(strDate.Carbon2Time()) {
			tag += 4
		}
		if tag != 0 {
			m = append(m, map[string]interface{}{
				"as_of_date": strDate.ToDateString(),
				"tag":        tag,
			})
		}

		strDate = strDate.AddDays(1)
	}

	return m
}
