package time

import (
	"fmt"
	"github.com/golang-module/carbon/v2"
	"testing"
)

func TestCarbonParse(t *testing.T) {
	birthday := "2022/05/18"
	fmt.Println(carbon.ParseByFormat(birthday, "2006/01/02").ToDateTimeString())
}

func TestCarbonCreateFromTimestamp(t *testing.T) {
	birthday := 1652400000
	age := int(carbon.CreateFromTimestamp(int64(birthday)).DiffInYears())
	fmt.Println(age)
}
