package config

import (
	"encoding/json"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var AppConfig map[string]interface{}

var IsDebug = true

func GetAppPath() (string, error) {
	//run模式下，得到debug(go run)时代码所在的真实路径
	if IsDebug {
		return os.Getwd()
	}

	//编译模式下，得到go build后的真实路径
	file, err := exec.LookPath(os.Args[0])
	if err != nil {
		return file, err
	}
	path, err := filepath.Abs(file)
	if err != nil {
		return path, err
	}

	index := strings.LastIndex(path, string(os.PathSeparator))

	return path[:index], nil
}

//LoadConfiguration 读取配置文件
func LoadConfig(file string) error {
	if AppConfig == nil {
		configFile, err := os.Open(file)
		defer configFile.Close()
		if err != nil {
			return err
		}
		jsonParser := json.NewDecoder(configFile)

		return jsonParser.Decode(&AppConfig)
	}

	return nil
}
