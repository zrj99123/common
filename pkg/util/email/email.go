package email

import (
	"crypto/tls"
	"github.com/jordan-wright/email"
	"gopkg.in/gomail.v2"
	"net/smtp"
)

const (
	// 邮件服务器地址
	MailHost = "smtp.163.com"
	// 端口
	MailPort = 25
	// 发送邮件用户账号
	MailUser = "******@163.com"
	// 授权密码
	MailPwd = "********"
)

func SendMessage(toUserEmail, message string) error {
	e := email.NewEmail()
	e.From = "your program <xxxxxxx@163.com>"
	e.To = []string{toUserEmail}
	e.Subject = "Error"
	e.HTML = []byte("Error Message: <b>" + message + "</b>")
	return e.SendWithTLS("smtp.163.com:465",
		smtp.PlainAuth("", "xxxxxxxx@163.com", "xxxxxx", "smtp.163.com"),
		&tls.Config{InsecureSkipVerify: true, ServerName: "smtp.163.com"})
}

func SendMessage2(mailAddress []string, subject string, body string) error {
	m := gomail.NewMessage()
	// 这种方式可以添加别名，即 nickname， 也可以直接用<code>m.SetHeader("From", MAIL_USER)</code>
	nickname := "gomail"
	m.SetHeader("From", nickname+"<"+MailUser+">")
	// 发送给多个用户
	m.SetHeader("To", mailAddress...)
	// 设置邮件主题
	m.SetHeader("Subject", subject)
	// 设置邮件正文
	m.SetBody("text/html", body)
	d := gomail.NewDialer(MailHost, MailPort, MailUser, MailPwd)
	// 发送邮件
	err := d.DialAndSend(m)
	return err
}
