package file

import (
	"bytes"
	"encoding/base64"
	"io"
	"mime/multipart"
)

//图片转换成base64的字符串
func Image2base64(file *multipart.FileHeader) string {
	f, err := file.Open()
	if err != nil {
		return ""
	}
	buf := bytes.NewBuffer(nil)
	if _, err = io.Copy(buf, f); err != nil {
		return ""
	}

	return base64.StdEncoding.EncodeToString(buf.Bytes())
}
