package file

import (
	"path"
	"path/filepath"
	"strings"
)

//return   fname不带后缀名  fSuffix
func GetFileNameAndSuffix(files string) (string, string) {
	fileNameWithSuffix := filepath.Base(files)
	fileSuffix := path.Ext(files)

	return strings.TrimSuffix(fileNameWithSuffix, fileSuffix), fileSuffix
}

//return   fname
func GetFileName(files string) string {
	return filepath.Base(files)
}

//return   fSuffix
func GetFileSuffix(files string) string {
	return path.Ext(files)
}

//2014-01-02-StockMarketData 获取表名称
func GetTableNameByFname(fName string) (string, string) {
	strs := strings.Split(fName, "-")
	if len(strs) < 4 {
		return "", ""
	}
	a := make([]string, 0)
	a = append(a, strs[len(strs)-1:]...)
	a = append(a, strs[:2]...)

	return SnakeString(strings.Join(a, "")), SnakeString(a[0])
}

//2014-01-02-StockMarketData 获取日期
func GetDateByFname(fName string) string {
	strs := strings.Split(fName, "-")
	if len(strs) < 4 {
		return ""
	}

	return strings.Join(strs[:3], "-")
}

/**
 * 驼峰转蛇形 snake string
 * @description XxYy to xx_yy , XxYY to xx_y_y
 * @date 2020/7/30
 * @param s 需要转换的字符串
 * @return string
 **/
func SnakeString(s string) string {
	data := make([]byte, 0, len(s)*2)
	j := false
	num := len(s)
	for i := 0; i < num; i++ {
		d := s[i]
		// or通过ASCII码进行大小写的转化
		// 65-90（A-Z），97-122（a-z）
		//判断如果字母为大写的A-Z就在前面拼接一个_
		if i > 0 && d >= 'A' && d <= 'Z' && j {
			data = append(data, '_')
		}
		if d != '_' {
			j = true
		}
		data = append(data, d)
	}
	//ToLower把大写字母统一转小写
	return strings.ToLower(string(data[:]))
}
