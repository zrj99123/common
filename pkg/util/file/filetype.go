package file

import (
	"path/filepath"
)

var (
	fileTypeMap = map[string]int{
		"doc":   1,
		"docx":  1,
		"docm":  1,
		"dot":   1,
		"dotx":  1,
		"dotm":  1,
		"odt":   1,
		"wpd":   1,
		"pages": 1,
		"gdoc":  1,

		"xl":      2,
		"xls":     2,
		"xlsx":    2,
		"xlsm":    2,
		"xlsb":    2,
		"xlam":    2,
		"xltx":    2,
		"xltm":    2,
		"xla":     2,
		"xlt":     2,
		"xlm":     2,
		"xlw":     2,
		"csv":     2,
		"numbers": 2,
		"gsheet":  2,

		"pdf": 3,

		"bmp":  4,
		"dib":  4,
		"jpg":  4,
		"jpeg": 4,
		"jpe":  4,
		"jfif": 4,
		"gif":  4,
		"tif":  4,
		"tiff": 4,
		"png":  4,
		"ico":  4,
		"svg":  4,

		"mid":  5,
		"mp3":  5,
		"m4a":  5,
		"ogg":  5,
		"flac": 5,
		"wav":  5,
		"amr":  5,
		"aac":  5,
		"aiff": 5,

		"mp4":  6,
		"m4v":  6,
		"mkv":  6,
		"webm": 6,
		"mov":  6,
		"avi":  6,
		"wmv":  6,
		"mpg":  6,
		"flv":  6,
		"3gp":  6,

		"7z":       7,
		"xz":       7,
		"bzip2":    7,
		"gzip":     7,
		"tar":      7,
		"zip":      7,
		"arj":      7,
		"cab":      7,
		"chm":      7,
		"cpio":     7,
		"cramfs":   7,
		"deb":      7,
		"dmg":      7,
		"fat":      7,
		"hfs":      7,
		"iso":      7,
		"lzh":      7,
		"lzma":     7,
		"mbr":      7,
		"msi":      7,
		"nsis":     7,
		"ntfs":     7,
		"rar":      7,
		"rpm":      7,
		"squashfs": 7,
		"udf":      7,
		"vhd":      7,
		"wim":      7,
		"xar":      7,
		"z":        7,

		"ppt":    8,
		"pptx":   8,
		"pptm":   8,
		"ppsx":   8,
		"pps":    8,
		"ppsm":   8,
		"potx":   8,
		"pot":    8,
		"potm":   8,
		"odp":    8,
		"key":    8,
		"gslide": 8,

		"txt": 9,
		"rtf": 9,
	}
)

func GetFileType(filename string) int {

	ext := filepath.Ext(filename)
	if len(ext) <= 1 {
		return 9
	}

	if t, ok := fileTypeMap[ext[1:]]; ok {
		return t
	}
	return 9
}
