package file

import (
	"testing"
)

func TestGetFileType(t *testing.T) {

	items := []struct {
		item string
		want int
	}{
		{
			item: "123.doc",
			want: 1,
		},
		{
			item: "123.docx",
			want: 1,
		},
		{
			item: "123.xlsx",
			want: 2,
		},
		{
			item: "123.xls",
			want: 2,
		},
		{
			item: "123.pdf",
			want: 3,
		},
		{
			item: "123.jpg",
			want: 4,
		},
		{
			item: "123.mp3",
			want: 5,
		},
		{
			item: "123.mp4",
			want: 6,
		},
	}

	for _, item := range items {
		got := GetFileType(item.item)
		if got != item.want {
			t.Errorf("item:%+v want:%d != result:%+v", item.item, item.want, got)
		}
	}
}
