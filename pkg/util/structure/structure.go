package structure

import (
	"github.com/jinzhu/copier"
	"reflect"
)

// Copy 结构体映射
func Copy(s, ts interface{}) error {
	return copier.Copy(ts, s)
}

// Struct转成map
func StructToMap(obj interface{}, tagName string) map[string]interface{} {
	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)

	var data = make(map[string]interface{})
	for i := 0; i < t.NumField(); i++ {
		keyName := t.Field(i).Tag.Get(tagName)
		if keyName != "" && keyName != "-" {
			data[keyName] = v.Field(i).Interface()
		}
	}
	return data
}
