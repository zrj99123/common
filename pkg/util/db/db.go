package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/lib/pq"
	"log"
	"time"
)

var (
	//不存在并发更新map所有不需要用sync
	rDBMap, wDBMap = make(map[string]*gorm.DB), make(map[string]*gorm.DB)
)

func InitDB(data map[string]string) error {
	connstr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		data["dbhost"], data["dbport"], data["dbuser"], data["dbpwd"], data["dbName"])

	log.Printf("Creating a new connection: %v", connstr)

	db, err := gorm.Open("postgres", connstr)
	if err != nil {
		return err
	}
	err = db.DB().Ping()
	if err != nil {
		return err
	}

	// 全局禁用表名复数
	db.SingularTable(true)

	//开启日志模式
	db.LogMode(true)

	db.DB().SetMaxIdleConns(100)
	db.DB().SetMaxOpenConns(1000)
	//增加新增时的回调函数
	db.Callback().Create().Replace("gorm:before_create", createSelfCall)

	rDBMap[data["dbName"]] = db
	wDBMap[data["dbName"]] = db
	return nil
}

//新增前的回调
func createSelfCall(scope *gorm.Scope) {
	if scope.HasColumn("create_time") {
		scope.SetColumn("create_time", time.Now())
	}
}

func GetReadBusinessDB() *gorm.DB {
	return rDBMap["db_business"]
}

func GetWriteBusinessDB() *gorm.DB {
	return wDBMap["db_business"]
}

func GetReadDataSourceDB() *gorm.DB {
	return rDBMap["db_datasource"]
}

func GetWriteDataSourceDB() *gorm.DB {
	return wDBMap["db_datasource"]
}

func GetReadPostgresDB() *gorm.DB {
	return rDBMap["postgres"]
}

func GetWritePostgresDB() *gorm.DB {
	return wDBMap["postgres"]
}
