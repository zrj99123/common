package conv

import (
	"fmt"
	"testing"
)

func TestFloat64Format(t *testing.T) {
	type args struct {
		value float64
		num   int
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "test",
			args: args{
				value: 10.22423333,
				num:   3,
			},
			want: 10.224,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Float64Format(tt.args.value, tt.args.num); got != tt.want {
				t.Errorf("Float64Format() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFloat64ToPercent(t *testing.T) {
	type args struct {
		f float64
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "test",
			args: args{
				f: 0.22423333,
			},
			want: "22.42%",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Float64ToPercent(tt.args.f); got != tt.want {
				t.Errorf("Float64ToPercent() = %s, want %s", got, tt.want)
			}
		})
	}
}

func TestTrimSpace(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{
			name: "test",
			args: args{
				" a a\n ",
			},
			want: "aa",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := TrimSpace(tt.args.str); got != tt.want {
				t.Errorf("TrimSpace() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatchingRDuration(t *testing.T) {
	type args struct {
		percent string
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		// TODO: Add test cases.
		{
			name: "test",
			args: args{
				percent: "10.00%",
			},
			want: 10.00,
		},
		{
			name: "test",
			args: args{
				percent: "0%",
			},
			want: 0.0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MatchingPercent(tt.args.percent); got != tt.want {
				t.Errorf("MatchingRDuration() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPercentToFloat64(t *testing.T) {
	type args struct {
		percent string
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		// TODO: Add test cases.
		{
			name: "test",
			args: args{
				percent: "10.00%",
			},
			want: 0.100,
		},
		{
			name: "test",
			args: args{
				percent: "0%",
			},
			want: 0.0,
		},
		{
			name: "test",
			args: args{
				percent: "0.08",
			},
			want: 0.09,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := PercentToFloat64(tt.args.percent); got != tt.want {
				t.Errorf("PercentToFloat64() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInt2Format(t *testing.T) {
	datas := []int{
		0,
		2,
		1,
		20,
		30,
	}

	for _, data := range datas {
		fmt.Println("aes before:", data, " aes after", data, SubLeftInt(data))
	}
}

func TestFloat64ToString(t *testing.T) {
	datas := []float64{
		1.1,
		2.2,
		3.3,
		20,
		30,
	}

	for _, data := range datas {
		fmt.Println("aes before:", data, " aes after", data, Float64ToString(data, 3))
	}
}
