package conv

import (
	"fmt"
	"testing"
)

func TestFloatRound6Decimal(t *testing.T) {
	datas := []float64{
		1.1234567,
		2.123,
		3.12345678,
		20,
		30,
	}

	for _, data := range datas {
		fmt.Println("aes before:", data, " aes after:", FloatRound6Decimal(data))
	}
}
