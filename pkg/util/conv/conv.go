package conv

import (
	"math"
	"regexp"
	"strconv"
)

func ParseStringSliceToUint64(s []string) []uint64 {
	iv := make([]uint64, len(s))
	for i, v := range s {
		iv[i], _ = strconv.ParseUint(v, 10, 64)
	}
	return iv
}

func Int64ToFloat64(i int64) float64 {
	s := strconv.FormatInt(i, 10)

	f, _ := strconv.ParseFloat(s, 64)
	return f
}

func FloatRound6Decimal(v float64) float64 {
	return math.Round(v*1e6) / 1e6
}

func FloatRound4Decimal(v float64) float64 {
	return math.Round(v*1e4) / 1e4
}

func FloatRound2Decimal(v float64) float64 {
	return math.Round(v*1e2) / 1e2
}

//return : year  month  day
func MatchingDuration(duration string) (int, int, int) {
	//查找数组
	re := regexp.MustCompile("[0-9]+")
	numberArr := re.FindAllString(duration, -1)
	number := 1
	if len(numberArr) >= 1 {
		number, _ = strconv.Atoi(numberArr[0])
	}

	//查找字母
	reg := regexp.MustCompile(`[a-z]+`)
	charArr := reg.FindAllString(duration, -1)
	if len(charArr) <= 0 {
		return 0, 0, 0
	}
	unit := charArr[0]

	switch unit {
	case "d":
		return 0, 0, number
	case "w":
		return 0, 0, 7 * number
	case "m":
		return 0, number, 0
	case "q":
		return 0, 3 * number, 0
	case "y":
		return number, 0, 0
	default:
		return 0, 0, 0
	}
}

//return : year  month  day
func MatchingRDuration(duration string) (int, int, int) {
	//查找数组
	re := regexp.MustCompile("[0-9]+")
	numberArr := re.FindAllString(duration, -1)
	number := 1
	if len(numberArr) >= 1 {
		number, _ = strconv.Atoi(numberArr[0])
	}

	//查找字母
	reg := regexp.MustCompile(`[a-z]+`)
	charArr := reg.FindAllString(duration, -1)
	if len(charArr) <= 0 {
		return 0, 0, 0
	}
	unit := charArr[0]

	switch unit {
	case "d":
		return 0, 0, number
	case "w":
		return 0, 0, 7 * number
	case "m":
		return 0, number, 0
	case "q":
		return 0, 3 * number, 0
	case "y":
		return number, 0, 0
	default:
		return 0, 0, 0
	}
}

//
func GetRDurationUnit(duration string) string {
	//查找字母
	reg := regexp.MustCompile(`[a-z]+`)
	charArr := reg.FindAllString(duration, -1)
	if len(charArr) <= 0 {
		return ""
	}
	unit := charArr[0]

	switch unit {
	case "d", "w", "m", "q", "y":
		return unit
	default:
		return ""
	}
}
