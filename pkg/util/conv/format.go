package conv

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

//截取int左侧第一位
func SubLeftInt(value int) string {
	rs := []rune(strconv.Itoa(value))

	return string(rs[:1])
}

func Float64Format(value float64, num int) float64 {
	format := "%." + strconv.Itoa(num) + "f"
	f, _ := strconv.ParseFloat(fmt.Sprintf(format, value), 64)

	return f
}

func Float64ToString(f float64, n ...int) string {
	var prec = 2
	if len(n) > 0 {
		prec = n[0]
	}
	return strconv.FormatFloat(f, 'f', prec, 64)
}

func String2Float64(s string) float64 {
	f, _ := strconv.ParseFloat(s, 64)
	return f
}

//float64 转成百分比
func Float64ToPercent(f float64) string {
	return strconv.FormatFloat(f*100.0, 'f', 2, 64) + "%"
}

//百分比 转成 float64
func PercentToFloat64(percent string) float64 {
	if strings.Contains(percent, "%") {
		return MatchingPercent(percent) / 100.0
	}
	return String2Float64(percent)
}

func TrimSpace(str string) string {
	//去除空格
	str = strings.Replace(str, " ", "", -1)

	// 去除换行符
	str = strings.Replace(str, "\n", "", -1)

	return str
}

//匹配字符串中的数字
func MatchingPercent(percent string) float64 {
	var number float64
	//查找数组
	re := regexp.MustCompile("[0-9]+")
	numberArr := re.FindAllString(percent, -1)

	if len(numberArr) >= 1 {
		number, _ = strconv.ParseFloat(numberArr[0], 64)
	}
	return number
}
