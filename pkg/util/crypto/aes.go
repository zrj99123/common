package crypto

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"errors"
	"fmt"
)

var defaultKey = "cherami906"

// AES加密
func EncodeString(src string, key ...string) string {
	k := defaultKey
	if len(key) > 0 && key[0] != "" {
		k = key[0]
	}
	// key 补为16的倍数字符
	k = fmt.Sprintf("%032s", k)

	encrypted, err := aesEncrypt([]byte(src), []byte(k))
	if err != nil {
		return ""
	}

	return base64.StdEncoding.EncodeToString(encrypted)
}

func EncodeStringByURLEncoding(src string, key ...string) string {
	k := defaultKey
	if len(key) > 0 && key[0] != "" {
		k = key[0]
	}
	// key 补为16的倍数字符
	k = fmt.Sprintf("%032s", k)

	encrypted, err := aesEncrypt([]byte(src), []byte(k))
	if err != nil {
		return ""
	}

	return base64.URLEncoding.EncodeToString(encrypted)
}

// AES解密
func DecodeString(src string, key ...string) string {
	k := defaultKey
	if len(key) > 0 && key[0] != "" {
		k = key[0]
	}
	// key 补为16的倍数字符
	k = fmt.Sprintf("%032s", k)

	encrypted, err := base64.StdEncoding.DecodeString(src)
	if err != nil {
		fmt.Println(err)
		return ""
	}
	bytesrc, err := aesDecrypt(encrypted, []byte(k))
	if err != nil {
		return ""
	}

	return string(bytesrc)
}

// AES解密
func DecodeStringByURLEncoding(src string, key ...string) string {
	k := defaultKey
	if len(key) > 0 && key[0] != "" {
		k = key[0]
	}
	// key 补为16的倍数字符
	k = fmt.Sprintf("%032s", k)

	encrypted, err := base64.URLEncoding.DecodeString(src)
	if err != nil {
		return ""
	}

	bytesrc, err := aesDecrypt(encrypted, []byte(k))
	if err != nil {
		return ""
	}

	return string(bytesrc)
}

func pKCS7Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func pKCS7UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

//AES加密,CBC
func aesEncrypt(origData, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	blockSize := block.BlockSize()
	origData = pKCS7Padding(origData, blockSize)
	blockMode := cipher.NewCBCEncrypter(block, key[:blockSize])
	crypted := make([]byte, len(origData))
	blockMode.CryptBlocks(crypted, origData)
	return crypted, nil
}

//AES解密
func aesDecrypt(crypted, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	blockSize := block.BlockSize()
	blockMode := cipher.NewCBCDecrypter(block, key[:blockSize])
	origData := make([]byte, len(crypted))

	if len(crypted) % blockMode.BlockSize() != 0 {
		return nil,errors.New("crypto/cipher: input not full blocks")
	}

	blockMode.CryptBlocks(origData, crypted)
	origData = pKCS7UnPadding(origData)
	return origData, nil
}
