package crypto

import (
	"fmt"
	"testing"
)

func TestEncodeString(t *testing.T) {
	datas := []string{
		"13526789870",
		"17322308652",
		"13802837489",
		"13697784642",
		"13168070508",
		"18688886130",
		"",
	}

	for _, data := range datas {
		fmt.Println("aes before:", data, " aes after", data, EncodeString(data))
	}
	return
}

func TestDecodeStringByURLEncoding(t *testing.T) {
	datas := []string{
		"Fb3WByyPKh9BbllSfUoYpoJ8umg_uYbucIubopc3_Wfz51KrYj6-8eMgGoy6yiHX",
		"Jj5yGibj0RJwdusIytDDiw==",
		"ZJ6w91uRKMWMJRYxW_G43g==",
		"C361GVfTtZhhOlfFdHcivJx3hYbvpQEZVqXsGRn1Hgk=",
		"C361GVfTtZhhOlfFdHcivNn11rbUvxqutUKvcGWx7jE=",
	}

	for _, data := range datas {
		fmt.Println("aes before:", data, " aes after", data, DecodeStringByURLEncoding(data))
	}
	return
}
