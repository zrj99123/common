package crypto

import (
	"bytes"
	"crypto/des"
	"encoding/base64"
	"errors"
)

var (
	defaultDesECBKey = "#s^uf33j55--;;9087810990"
)

func PKCS5Padding(cipherText []byte, blockSize int) []byte {
	padding := blockSize - (len(cipherText) % blockSize)
	padText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(cipherText, padText...)
}

func PKCS5UnPadding(origData []byte) []byte {
	length := len(origData)
	unPadding := int(origData[length-1])
	return origData[:(length - unPadding)]
}

func PKCS5UnPadding2(origData []byte) ([]byte,error) {
	length := len(origData)
	unPadding := int(origData[length-1])
	s := length - unPadding
	if s <= 0 || s > length {
		return nil,errors.New("slice out of range")
	}
	return origData[:(length - unPadding)],nil
}

func TripleDesECBEncrypt(src string, key ...string) (string, error) {

	k := defaultDesECBKey
	if len(key) > 0 && key[0] != "" {
		k = key[0]
	}

	block, err := des.NewTripleDESCipher([]byte(k))
	if err != nil {
		return "", err
	}
	bs := block.BlockSize()
	origData := PKCS5Padding([]byte(src), bs)
	if len(origData)%bs != 0 {
		return "", errors.New("need a multiple of the block size")
	}

	out := make([]byte, len(origData))
	dst := out

	for len(origData) > 0 {
		block.Encrypt(dst, origData[:bs])
		origData = origData[bs:]
		dst = dst[bs:]
	}

	return base64.StdEncoding.EncodeToString(out), nil
}

func TripleDesECBDecrypt(src string, key ...string) (string, error) {

	k := defaultDesECBKey
	if len(key) > 0 && key[0] != "" {
		k = key[0]
	}

	decodeSrc, err := base64.StdEncoding.DecodeString(src)
	if err != nil {
		return "", err
	}

	block, err := des.NewTripleDESCipher([]byte(k))
	if err != nil {
		return "", err
	}
	bs := block.BlockSize()
	if len(decodeSrc)%bs != 0 {
		return "", errors.New("crypto/cipher: input not full blocks")
	}
	out := make([]byte, len(decodeSrc))
	dst := out
	for len(decodeSrc) > 0 {
		block.Decrypt(dst, decodeSrc[:bs])
		decodeSrc = decodeSrc[bs:]
		dst = dst[bs:]
	}
	out,err = PKCS5UnPadding2(out)
	if err != nil {
		return "",err
	}
	return string(out), nil
}
