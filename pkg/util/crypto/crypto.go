package crypto

import (
	"fmt"
	"gitee.com/zrj99123/common/pkg/errors"
	"regexp"
	"strings"

	cregex "github.com/mingrammer/commonregex"
)

func EncryptMobileNumber(mobile string, key ...string) (string, error) {

	var (
		desECBKey, aesKey []string
	)

	if len(key) >= 1 {
		desECBKey = key[0:]
	}

	if len(key) >= 2 {
		aesKey = key[1:]
	}

	encode, err := TripleDesECBEncrypt(mobile, desECBKey...)
	if err != nil {
		return "", err
	}
	doubleEncode := EncodeString(encode, aesKey...)

	return doubleEncode, nil
}

func DecryptMobileNumber(encodeMobile string, key ...string) (string, error) {

	var (
		desECBKey, aesKey []string
	)

	if len(key) >= 1 {
		desECBKey = key[0:]
	}

	if len(key) >= 2 {
		aesKey = key[1:]
	}

	decodeMobile := DecodeString(encodeMobile, aesKey...)

	if decodeMobile == "" {
		return "", errors.New(fmt.Sprintf("encode mobile format err"))
	}

	mobile, err := TripleDesECBDecrypt(decodeMobile, desECBKey...)
	if err != nil {
		return "", err
	}

	return mobile, nil
}

func DecryptMobileNumberSafe(encodeMobile string, key ...string) (string, error) {
	var (
		err          error
		decodeMobile string
	)
	defer func() {
		if e := recover(); e != nil {
			err = e.(error)
		}
	}()

	decodeMobile, err = DecryptMobileNumber(encodeMobile, key...)
	if err != nil {
		decodeMobile = DecodeStringByURLEncoding(encodeMobile, key...)
		if decodeMobile != "" {
			err = nil
		}
	}

	return decodeMobile, err
}

func DecryptMobileNumber2(phone string) (string, error) {

	// 支持的手机号码格式
	// 1. 021-50185500
	// 2. 0291-50185500
	// 3. 15326308652
	// 4. +15326308652
	// phone 为手机号码的时候,直接返回
	if regexPhone := cregex.Phones(phone); regexPhone != nil && regexPhone[0] == phone {
		return phone, nil
	}

	// 尝试解密方法一
	decodedPhone := DecodeStringByURLEncoding(phone)
	if regexPhone := cregex.Phones(decodedPhone); regexPhone != nil && regexPhone[0] == decodedPhone {
		return decodedPhone, nil
	}

	// 尝试解密方法二
	var err error
	decodedPhone = DecodeStringByURLEncoding(phone)
	if decodedPhone != "" {
		if decodedPhone, err = TripleDesECBDecrypt(decodedPhone); err != nil {
			if regexPhone := cregex.Phones(decodedPhone); regexPhone != nil && regexPhone[0] == decodedPhone {
				return decodedPhone, nil
			}
		}
	}

	// 尝试解密方法三
	decodedPhone = DecodeString(phone)
	if decodedPhone != "" {
		if decodedPhone, err = TripleDesECBDecrypt(decodedPhone); err != nil {
			if regexPhone := cregex.Phones(decodedPhone); regexPhone != nil && regexPhone[0] == decodedPhone {
				return decodedPhone, nil
			}
		}
	}

	// 尝试解密方法四
	decodedPhone, _ = TripleDesECBDecrypt(phone)
	if decodedPhone != "" {
		if regexPhone := cregex.Phones(decodedPhone); regexPhone != nil && regexPhone[0] == decodedPhone {
			return decodedPhone, nil
		}
	}

	return "", errors.New(fmt.Sprintf("decode mobile number error"))
}

func DecodeIdentification(encodeNumber string) string {

	var (
		finished                bool
		originEncodeNumber      = encodeNumber
		alphaNumericRegexString = "^[a-zA-Z0-9]+$"
		alphaNumericRegex       = regexp.MustCompile(alphaNumericRegexString)
	)

	for i := 0; i < 5; i++ {

		if encodeNumber == "" {
			return encodeNumber
		}

		if alphaNumericRegex.MatchString(encodeNumber) {
			finished = true
			break
		}

		encodeNumber = DecodeStringByURLEncoding(encodeNumber)
	}

	if !finished {
		return originEncodeNumber
	}

	return encodeNumber
}

func DecodeAccount(encodeNumber string) string {

	var (
		finished           bool
		originEncodeNumber = encodeNumber
		numberRegexString  = "^[0-9]+$"
		numberRegex        = regexp.MustCompile(numberRegexString)
	)

	for i := 0; i < 5; i++ {

		if encodeNumber == "" {
			return encodeNumber
		}

		if numberRegex.MatchString(encodeNumber) {
			finished = true
			break
		}

		encodeNumber = DecodeStringByURLEncoding(encodeNumber)
	}

	if !finished {
		return originEncodeNumber
	}

	return encodeNumber
}

func TrimSpaceAndToUpperDefaultIC(defaultIC string) string {
	data := DecodeStringByURLEncoding(defaultIC)
	if data == "" {
		return data
	}

	return strings.ToUpper(strings.TrimSpace(data))
}
