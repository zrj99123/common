package crypto

import (
	"testing"
)

func TestEncryptMobileNumber(t *testing.T) {
	items := []struct {
		item string
		want string
	}{
		{
			item: "17322308652",
			want: "PLh23sRwi05VBH0KV48DXju43ClqnfnNZ91RKoEMG2g=",
		},
		{
			item: "13823937148",
			want: "vh3djBJLPUCRjsollLdvkcsZCkA5BXFYHNcCQ0TNZLo=",
		},
	}

	for _, item := range items {
		if got, err := EncryptMobileNumber(item.item); err != nil {
			t.Errorf("item:%+v want:%s got error:%+v", item.item, item.want, err)
		} else {
			if got != item.want {
				t.Errorf("item:%+v want:%s != result:%+v", item.item, item.want, got)
			}
		}
	}
}

//go test *.go -v -run TestEncryptMobileNumber2
func TestEncryptMobileNumber2(t *testing.T) {
	items := []struct {
		item string
		want string
	}{
		{
			item: "18926089907",
			want: "ztDBdHHJd0e4c38Un-dBNw==",
		},
		{
			item: "2271b69f-649f-4c2f-b780-7085408418c9",
			want: "ztDBdHHJd0e4c38Un-dBNw==",
		},
	}

	for _, item := range items {
		if err := EncodeStringByURLEncoding(item.item); err != "" {
			t.Errorf("item:%+v want:%s got error:%+v", item.item, item.want, err)
		}
	}
}

//go test *.go -v -run TestDecryptMobileNumber
func TestDecryptMobileNumber(t *testing.T) {

	items := []struct {
		item string
		want string
	}{
		//{
		//	item: "PLh23sRwi05VBH0KV48DXju43ClqnfnNZ91RKoEMG2g=",
		//	want: "17322308652",
		//},
		//{
		//	item: "vh3djBJLPUCRjsollLdvkcsZCkA5BXFYHNcCQ0TNZLo=",
		//	want: "13823937148",
		//},
		//{
		//	item: "DEKRMwo1KDQScL37zzSWfPjNCKBqhn73JsguO3mA_00=",
		//	want: "18509208419",
		//},
		//{
		//	item: "qu9o7C-w8Y_4xXtNgDEf4VfvpT51jRB78HgEnu4vqgA=",
		//	want: "13556005588",
		//},
		//{
		//	item: "n+AUSqf/qF7/qsjczNO72PQhdXIo2VqYMhROg76yfbI=",
		//	want: "17322308650",
		//},
		//{
		//	item: "lIeHN-pMDgXi_rXU_wjZvw==",
		//	want: "17322308652",
		//},
		//{
		//	item: "7Zt91b0fahNKWgbbYYf8FA==",
		//	want: "17322308652",
		//},
		//{
		//	item: "q3Z2B6wGTDKw58chYV-w_NZ1ckoEWBEH93CEh9R2EEo=",
		//	want: "17322308652",
		//},
		{
			item: "rWiOoon3Gonp3PxFNH_4Kw==",
			want: "17322308652",
		},
		{
			item: "ztDBdHHJd0e4c38Un+dBNw==",
			want: "18926089907",
		},
	}

	for _, item := range items {
		if got, err := DecryptMobileNumber(item.item); err != nil {
			t.Errorf("item:%+v want:%s got error:%+v", item.item, item.want, err)
		} else {
			if got != item.want {
				t.Errorf("item:%+v want:%s != result:%+v", item.item, item.want, got)
			}
		}
	}
}

//go test *.go -v -run TestDecryptMobileNumber2
func TestDecryptMobileNumber2(t *testing.T) {

	items := []struct {
		item string
		want string
	}{
		//{
		//	item: "PLh23sRwi05VBH0KV48DXju43ClqnfnNZ91RKoEMG2g=",
		//	want: "17322308652",
		//},
		//{
		//	item: "vh3djBJLPUCRjsollLdvkcsZCkA5BXFYHNcCQ0TNZLo=",
		//	want: "13823937148",
		//},
		//{
		//	item: "DEKRMwo1KDQScL37zzSWfPjNCKBqhn73JsguO3mA_00=",
		//	want: "18509208419",
		//},
		//{
		//	item: "qu9o7C-w8Y_4xXtNgDEf4VfvpT51jRB78HgEnu4vqgA=",
		//	want: "13556005588",
		//},
		//{
		//	item: "n+AUSqf/qF7/qsjczNO72PQhdXIo2VqYMhROg76yfbI=",
		//	want: "17322308650",
		//},
		//{
		//	item: "lIeHN-pMDgXi_rXU_wjZvw==",
		//	want: "17322308652",
		//},
		//{
		//	item: "7Zt91b0fahNKWgbbYYf8FA==",
		//	want: "17322308652",
		//},
		//{
		//	item: "q3Z2B6wGTDKw58chYV-w_NZ1ckoEWBEH93CEh9R2EEo=",
		//	want: "17322308652",
		//},
		{
			item: "rWiOoon3Gonp3PxFNH_4Kw==",
			want: "17322308652",
		},
		{
			item: "ztDBdHHJd0e4c38Un-dBNw==",
			want: "18926089907",
		},
		{
			item: "wocvW-uaJiNDO2rppoWooRq5FoBE98191LXyafF0qUffE-ri9tMpIBQlu78Ay5oc",
			want: "18926089907",
		},
	}

	for _, item := range items {
		if err := DecodeStringByURLEncoding(item.item); err != "" {
			t.Errorf("item:%+v want:%s got error:%+v", item.item, item.want, err)
		}
	}
}

//go test *.go -v -run TestDecryptIdentifications
func TestDecryptIdentifications(t *testing.T) {

	items := []struct {
		item string
		want string
	}{
		{
			item: "pTtuAn7qPbmuCbcdBix7FaC-0V_dmRodGHUqPUfbH9Q=",
			want: "422201197901015920",
		},
		{
			item: "NSMF_mCJotDzPka-yanm-rJjtMJODPPgnimWGiKD0bn22of2Ggn_bmhR2ofDc9cE",
			want: "422201197901015920",
		},
	}

	for _, item := range items {
		if got := DecodeStringByURLEncoding(item.item); got == "" {
			t.Errorf("item:%+v want:%s got error", item.item, item.want)
		} else {
			if got != item.want {
				t.Errorf("item:%+v want:%s got error", item.item, item.want)
			}
			t.Errorf("item:%+v want:%s got error", item.item, item.want)
		}
	}
}
