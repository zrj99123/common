package crypto

import (
	"fmt"
	"testing"
)

var (
	key = "#s^uf33j55--;;9087810990"
)

func TestTripleEcbDesDecrypt(t *testing.T) {
	datas := []string{
		"ZnSLg7nwZQzAKCtTAOMb4w==",
	}

	for _, data := range datas {
		if decodeString, err := TripleDesECBDecrypt(data, key); err != nil {
			t.Errorf("decode err:%+v", err)
		} else {
			fmt.Println("aes before:", data, " aes after", string(decodeString))
		}
	}
	return
}
