package redis

import (
	"gitee.com/zrj99123/common/pkg/broker"
	"github.com/go-redis/redis"
	"github.com/google/uuid"
	"time"
)

type redisBroker struct {
	opts *broker.Options
	conn *redis.Client
}

func NewBroker(opts *broker.Options) broker.Broker {
	return &redisBroker{
		opts: opts,
	}
}

func (r *redisBroker) Options() *broker.Options {
	return r.opts
}

func (r *redisBroker) NewProducer() (broker.Producer, error) {
	err := r.connect()

	producer := &Producer{conn: r.conn}
	return producer, err
}

func (r *redisBroker) NewConsumer(opts ...broker.SubscribeOption) (broker.Consumer, error) {

	err := r.connect()
	opt := broker.SubscribeOptions{
		GroupID: uuid.New().String(),
		AutoAck: true,
	}
	for _, o := range opts {
		o(&opt)
	}
	return &Consumer{
		conn: r.conn,
	}, err
}

func (r *redisBroker) connect() error {

	var (
		addr = ":6379"
	)

	if len(r.opts.Addr) > 0 {
		addr = r.opts.Addr[0]
	}

	r.conn = redis.NewClient(&redis.Options{
		Addr:         addr,
		DialTimeout:  10 * time.Second,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		PoolSize:     10,
		PoolTimeout:  30 * time.Second,
	})

	return nil
}

func (r *redisBroker) Close() error {
	return r.conn.Close()
}

func (r *redisBroker) Type() string {
	return "redis"
}

type Producer struct {
	conn *redis.Client
}

func (pr *Producer) Publish(channel string, msg *broker.Message) error {

	return pr.conn.Publish(channel, string(msg.Body)).Err()
}

type Consumer struct {
	channel string
	conn    *redis.Client
}

func (cr *Consumer) GroupID() string {
	return ""
}

func (cr *Consumer) Topic() string {

	return cr.channel
}

func (cr *Consumer) Subscribe(topic string, handler broker.Handler) error {

	pubSub := cr.conn.Subscribe(topic)

	for msg := range pubSub.Channel() {

		m := &broker.Message{
			Body: []byte(msg.Payload),
		}
		evt := &Event{
			t: msg.Channel, m: m,
		}
		_ = handler(evt)
	}

	return nil
}

func (cr *Consumer) Unsubscribe() error {

	return cr.conn.Close()
}

func (cr *Consumer) resubscribe() {

	return
}
