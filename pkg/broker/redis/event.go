package redis

import (
	"gitee.com/zrj99123/common/pkg/broker"
)

type Event struct {
	m   *broker.Message
	t   string
	err error
}

func (evt *Event) Ack() error {
	return nil
}

func (evt *Event) Error() error {
	return evt.err
}

func (evt *Event) Topic() string {
	return evt.t
}

func (evt *Event) Message() *broker.Message {
	return evt.m
}
