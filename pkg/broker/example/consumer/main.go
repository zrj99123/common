package main

import (
	"fmt"
	"gitee.com/zrj99123/common/pkg/broker"
	"gitee.com/zrj99123/common/pkg/broker/redis"
	"os"
)

func main() {

	ch := "test_channel"
	brk := redis.NewBroker(&broker.Options{
		Addr: []string{"47.106.122.157:9736"},
		RedisMq: &broker.RedisMq{
			Channel: ch,
		},
	})

	consumer, err := brk.NewConsumer()
	if err != nil {
		fmt.Println("create producer err:", err)
		os.Exit(-1)
	}

	err = consumer.Subscribe(ch, func(evt broker.Event) error {
		fmt.Println(string(evt.Message().Body))
		return nil
	})

	fmt.Println(brk.Close())
}
