package main

import (
	"fmt"
	"gitee.com/zrj99123/common/pkg/broker"
	"gitee.com/zrj99123/common/pkg/broker/redis"
	"os"
	"time"
)

func main() {

	ch := "test_channel"
	brk := redis.NewBroker(&broker.Options{
		Addr: []string{"47.106.122.157:9736"},
		RedisMq: &broker.RedisMq{
			Channel: ch,
		},
	})

	producer, err := brk.NewProducer()
	if err != nil {
		fmt.Println("create producer err:", err)
		os.Exit(-1)
	}

	for i := 0; i < 1000; i++ {
		err = producer.Publish(ch, &broker.Message{
			Body: []byte(fmt.Sprintf("这是发布的第%v条消息", i)),
		})
		time.Sleep(time.Second)
	}

	fmt.Println(brk.Close())
}
