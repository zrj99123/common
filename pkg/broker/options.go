package broker

import (
	"crypto/tls"
	"github.com/IBM/sarama"
	"github.com/streadway/amqp"
)

type Options struct {
	Addr         []string
	Kafka        *sarama.Config
	RabbitMq     *RabbitMq
	RedisMq      *RedisMq
	ErrorHandler Handler
}

type Option func(*Options)

type SubscribeOptions struct {
	AutoAck bool
	GroupID string
}

type SubscribeOption func(*SubscribeOptions)

type RabbitMq struct {
	ExchangeType    string
	ExchangeKey     string
	DurableExchange bool
	PrefetchCount   int
	PrefetchGlobal  bool

	Secure         bool
	TLSConfig      *tls.Config
	Authentication amqp.Authentication
}

type RedisMq struct {
	Channel string
	DB      int
}

//https://github.com/gaopengfei123123/go_study/blob/master/src/mq_demo/mqhandler/rabbitmq/server.go
